//
//  grid_function.cpp
//  partest
//
//  Created by Антон Кудряшов on 16/01/17.
//  Copyright © 2017 Антон Кудряшов. All rights reserved.
//

#include "grid_function.h"

#include "layer.h"
#include "mpi_utils.h"

void FindOtherDirections(const int direction, int* direction2, int* direction3) {
    
    *direction2 = direction+1 > 3 ? (direction+1)%3 : direction+1;
    *direction3 = direction+2 > 3 ? (direction+2)%3 : direction+2;
    if (*direction3 < *direction2) {
        std::swap(*direction2, *direction3);
    }
}

#ifdef PARALLEL_MODE
void ArrayedFunc::UpdateBounds(const Control::Thread& thread) {
    
    for (int i = 1; i <= 3; ++i) {
        UpdateOuter(i, thread);
    }
    for (int i = 1; i <= 3; ++i) {
        UpdateSides(i, thread);
    }
    for (int i = 1; i <= 3; ++i) {
        UpdateEdges(i, thread);
    }
    for (int i = 1; i <= 3 ; ++i) {
        UpdateCorners(i, thread);
    }
    for (int i = 1; i <= 3 ; ++i) {
        UpdateAddition(i, thread);
    }
}
void ArrayedFunc::UpdateOuter(int direction, const Control::Thread& thread) {
    
    const int rank = thread.Rank(direction);
    const int size = thread.P(direction);
    Bound left = GetBound(direction, true);
    Bound right = GetBound(direction, false);
    
    if ((rank > 0 && rank < size - 1) || (size > 1 && left.type == BoundCyclic) || grid.N_loc(direction) == 1) {
        return;
    }
    int direction2, direction3;
    FindOtherDirections(direction, &direction2, &direction3);
    
    GridIndex firstI, lastI;
    gridType currentGridType = GridType();
    if (currentGridType > 0) {
        firstI.SetShift(true, currentGridType);
        lastI.SetShift(true, currentGridType);
    }
    firstI.SetI(0, direction);
    lastI.SetI(grid.N_loc(direction) - 1, direction);
    for (int j=0; j < grid.N_loc(direction2); ++j) {
        for (int k=0; k < grid.N_loc(direction3); ++k) {
            firstI.SetI(j, direction2);
            firstI.SetI(k, direction3);
            lastI.SetI(j, direction2);
            lastI.SetI(k, direction3);
            
            switch (left.type) {
                case BoundDirichlet:{
                    this->operator[](firstI) = left.Value(j, k);
                }
                    break;
                case BoundNeumann: {
                    GridIndex next = firstI;
                    grid.MoveNext(&next, direction);
                    this->operator[](firstI) =
                    this->operator()(next) - left.Value(j, k)*grid.H(direction);
                }
                    break;
                case BoundCyclic:
                    break;
                default: {
                    std::cerr << "unknown bound type" << left.type << std::endl;
                    std::terminate();
                }
                    break;
            }
            switch (right.type) {
                case BoundDirichlet:
                    this->operator[](lastI) = right.Value(j, k);
                    break;
                case BoundNeumann: {
                    GridIndex prev = lastI;
                    grid.MovePrev(&prev, direction);
                    this->operator[](lastI) =
                    this->operator()(prev) + right.Value(j, k)*grid.H(direction);
                }
                    break;
                case BoundCyclic: {
                    if (size == 1) {
                        this->operator[](lastI) = this->operator()(firstI);
                    }
                }
                    break;
                default: {
                    std::cerr << "unknown bound type" << right.type << std::endl;
                    std::terminate();
                }
                    break;
            }
        } //end for k
    } //end for j
}
void ArrayedFunc::UpdateSides(int direction, const Control::Thread& thread) {

    if (thread.P(direction) == 1) {
        // there is no exchanges
        return;
    }
    const int rank = thread.Rank(direction);
    const int size = thread.P(direction);
    MPI_Comm comm = thread.Commutation(direction);
    
    const int mextra = (int)container_.extra();
    int direction2, direction3;
    FindOtherDirections(direction, &direction2, &direction3);
    
    // main exchange
    GridIndex firstI, lastI;
    gridType currentGridType = GridType();
    if (currentGridType > 0) {
        firstI.SetShift(true, currentGridType);
        lastI.SetShift(true, currentGridType);
    }
    
    int destLeft = rank - 1;
    int destRight = rank + 1;
    {
        Bound left = GetBound(direction, true);
        if (rank == 0) {
            destLeft = left.type == BoundCyclic ? size - 1 : MPI_PROC_NULL;
        }
        if (rank == size - 1) {
            destRight = left.type == BoundCyclic ? 0 : MPI_PROC_NULL;
        }
    }
    BoundLayer leftLayer(grid.N_loc(direction2), grid.N_loc(direction3));
    BoundLayer rightLayer(grid.N_loc(direction2), grid.N_loc(direction3));
    const int Nbuf = leftLayer.size();
    for (int e = 0; e < mextra; ++e) {
        firstI.SetI(e + 1, direction);
        lastI.SetI(grid.N_loc(direction) - 2 - e, direction);
        
        for (int j=0; j < grid.N_loc(direction2); ++j) {
            for (int k=0; k < grid.N_loc(direction3); ++k) {
                firstI.SetI(j, direction2);
                firstI.SetI(k, direction3);
                lastI.SetI(j, direction2);
                lastI.SetI(k, direction3);
                if (destLeft != MPI_PROC_NULL) {
                    leftLayer.SetValue(j, k, this->operator()(firstI));
                }
                if (destRight != MPI_PROC_NULL) {
                    rightLayer.SetValue(j, k, this->operator()(lastI));
                }
            }
        }
        MPI_Status lStatus, rStatus;
        if (rank % 2 == 0) {
            MPI_Sendrecv_replace(leftLayer.GetContainer(), Nbuf, MPI_DOUBLE,
                                 destLeft, kGFTagLeftInner,
                                 destLeft, kGFTagRightInner,
                                 comm, &lStatus);
            MPI_Sendrecv_replace(rightLayer.GetContainer(), Nbuf, MPI_DOUBLE,
                                 destRight, kGFTagRightInner,
                                 destRight, kGFTagLeftInner,
                                 comm, &rStatus);
        } else {
            MPI_Sendrecv_replace(rightLayer.GetContainer(), Nbuf, MPI_DOUBLE,
                                 destRight, kGFTagRightInner,
                                 destRight, kGFTagLeftInner,
                                 comm, &rStatus);
            MPI_Sendrecv_replace(leftLayer.GetContainer(), Nbuf, MPI_DOUBLE,
                                 destLeft, kGFTagLeftInner,
                                 destLeft, kGFTagRightInner,
                                 comm, &lStatus);
        }
        
        firstI.SetI(-(e + 1), direction);
        lastI.SetI(grid.N_loc(direction) + e, direction);
        for (int j=0; j < grid.N_loc(direction2); ++j) {
            for (int k=0; k < grid.N_loc(direction3); ++k) {
                firstI.SetI(j, direction2);
                firstI.SetI(k, direction3);
                lastI.SetI(j, direction2);
                lastI.SetI(k, direction3);
                if (destLeft != MPI_PROC_NULL) {
                    this->operator[](firstI) = leftLayer.Value(j, k);
                }
                if (destRight != MPI_PROC_NULL) {
                    this->operator[](lastI) = rightLayer.Value(j, k);
                }
            }
        }
    } // end for e
    MPI_Barrier(comm);
}
void ArrayedFunc::UpdateAddition(int direction, const Control::Thread& thread) {
    
    if (thread.P(direction) == 1) {
        // there is no exchanges
        return;
    }
    const int rank = thread.Rank(direction);
    const int size = thread.P(direction);
    MPI_Comm comm = thread.Commutation(direction);
    
    int direction2, direction3;
    FindOtherDirections(direction, &direction2, &direction3);
    
    // main exchange
    if (rank == 0) {
        for (int i = 1; i < size; ++i) {
            BoundLayer* layer = layers_.Layer(i, direction);
            MPI_Status status;
            MPI_Recv(layer->GetContainer(), layer->size(), MPI_DOUBLE, i,
                     kGFTagAddition, comm, &status);
        }
    }
    else {
        GridIndex lastI;
        gridType currentGridType = GridType();
        if (currentGridType > 0) {
            lastI.SetShift(true, currentGridType);
        }
        BoundLayer layer(grid.N_loc(direction2), 2*grid.N_loc(direction3));
        lastI.SetI(grid.N_loc(direction)-2 , direction);
        for (int i = 0; i < grid.N_loc(direction2); i++) {
            lastI.SetI(i , direction2);
            for (int j = 0; j < grid.N_loc(direction3); j++) {
                lastI.SetI(j , direction3);
                layer.SetValue(i, j, this->operator()(lastI));
            }
        }
        lastI.SetI(grid.N_loc(direction)-1 , direction);
        for (int i = 0; i < grid.N_loc(direction2); i++) {
            lastI.SetI(i , direction2);
            for (int j = 0; j < grid.N_loc(direction3); j++) {
                lastI.SetI(j , direction3);
                layer.SetValue(i, j + grid.N_loc(direction3), this->operator()(lastI));
            }
        }
        MPI_Send(layer.GetContainer(), layer.size(), MPI_DOUBLE, 0,
                 kGFTagAddition, comm);
    }
    MPI_Barrier(comm);
}
void ArrayedFunc::UpdateEdges(int direction, const Control::Thread& thread) {
    
    if (thread.P(direction) == 1) {
        // there is no exchanges
        return;
    }
    const int rank = thread.Rank(direction);
    const int size = thread.P(direction);
    MPI_Comm comm = thread.Commutation(direction);
    
    if (container_.extra() > 1) {
        std::cerr << "doesn't work for for extra = " << container_.extra();
        std::cerr << std::endl;
        std::terminate();
    }
    int direction2, direction3;
    FindOtherDirections(direction, &direction2, &direction3);
    
    GridIndex firstI, lastI;
    gridType currentGridType = GridType();
    if (currentGridType > 0) {
        firstI.SetShift(true, currentGridType);
        lastI.SetShift(true, currentGridType);
    }
    int destLeft = rank - 1;
    int destRight = rank + 1;
    {
        Bound left = GetBound(direction, true);
        if (rank == 0) {
            destLeft = left.type == BoundCyclic ? size - 1 : MPI_PROC_NULL;
        }
        if (rank == size - 1) {
            destRight = left.type == BoundCyclic ? 0 : MPI_PROC_NULL;
        }
    }
    BoundLayer leftLayer(2, grid.N_loc(direction2) + grid.N_loc(direction3));
    BoundLayer rightLayer(2, grid.N_loc(direction2) + grid.N_loc(direction3));
    const int Nbuf = leftLayer.size();
    firstI.SetI(1, direction);
    lastI.SetI(grid.N_loc(direction) - 2, direction);
    for (int s=0; s < 2; ++s) {
        {
            int k = s == 0 ? -1 : grid.N_loc(direction3);
            firstI.SetI(k, direction3);
            lastI.SetI(k, direction3);
            for (int j=0; j < grid.N_loc(direction2); ++j) {
                firstI.SetI(j, direction2);
                lastI.SetI(j, direction2);
                if (destLeft != MPI_PROC_NULL) {
                    leftLayer.SetValue(s, j, this->operator()(firstI));
                }
                if (destRight != MPI_PROC_NULL) {
                    rightLayer.SetValue(s, j, this->operator()(lastI));
                }
            }
        }
        {
            int j = s == 0 ? -1 : grid.N_loc(direction2);
            firstI.SetI(j, direction2);
            lastI.SetI(j, direction2);
            for (int k=0; k < grid.N_loc(direction3); ++k) {
                firstI.SetI(k, direction3);
                lastI.SetI(k, direction3);
                if (destLeft != MPI_PROC_NULL) {
                    leftLayer.SetValue(s, grid.N_loc(direction2) + k, this->operator()(firstI));
                }
                if (destRight != MPI_PROC_NULL) {
                    rightLayer.SetValue(s, grid.N_loc(direction2) + k, this->operator()(lastI));
                }
            }
        }
    }
    MPI_Status lStatus, rStatus;
    if (rank % 2 == 0) {
        MPI_Sendrecv_replace(leftLayer.GetContainer(), Nbuf, MPI_DOUBLE,
                             destLeft, kGFTagLeftInner,
                             destLeft, kGFTagRightInner,
                             comm, &lStatus);
        MPI_Sendrecv_replace(rightLayer.GetContainer(), Nbuf, MPI_DOUBLE,
                             destRight, kGFTagRightInner,
                             destRight, kGFTagLeftInner,
                             comm, &rStatus);
    } else {
        MPI_Sendrecv_replace(rightLayer.GetContainer(), Nbuf, MPI_DOUBLE,
                             destRight, kGFTagRightInner,
                             destRight, kGFTagLeftInner,
                             comm, &rStatus);
        MPI_Sendrecv_replace(leftLayer.GetContainer(), Nbuf, MPI_DOUBLE,
                             destLeft, kGFTagLeftInner,
                             destLeft, kGFTagRightInner,
                             comm, &lStatus);
    }
    
    firstI.SetI(-1, direction);
    lastI.SetI(grid.N_loc(direction), direction);
    for (int s=0; s < 2; ++s) {
        {
            int k = s == 0 ? -1 : grid.N_loc(direction3);
            firstI.SetI(k, direction3);
            lastI.SetI(k, direction3);
            for (int j=0; j < grid.N_loc(direction2); ++j) {
                firstI.SetI(j, direction2);
                lastI.SetI(j, direction2);
                if (destLeft != MPI_PROC_NULL) {
                    this->operator[](firstI) = leftLayer.Value(s, j);
                }
                if (destRight != MPI_PROC_NULL) {
                    this->operator[](lastI) = rightLayer.Value(s, j);
                }
            }
        }
        {
            int j = s == 0 ? -1 : grid.N_loc(direction2);
            firstI.SetI(j, direction2);
            lastI.SetI(j, direction2);
            for (int k=0; k < grid.N_loc(direction3); ++k) {
                firstI.SetI(k, direction3);
                lastI.SetI(k, direction3);
                if (destLeft != MPI_PROC_NULL) {
                    this->operator[](firstI) = leftLayer.Value(s, grid.N_loc(direction2) + k);
                }
                if (destRight != MPI_PROC_NULL) {
                    this->operator[](lastI) = rightLayer.Value(s, grid.N_loc(direction2) + k);
                }
            }
        }
    }
    MPI_Barrier(comm);
}
void ArrayedFunc::UpdateCorners(int direction, const Control::Thread& thread) {
    
    if (thread.P(direction) == 1) {
        // there is no exchanges
        return;
    }
    const int rank = thread.Rank(direction);
    const int size = thread.P(direction);
    MPI_Comm comm = thread.Commutation(direction);
    
    if (container_.extra() > 1) {
        std::cerr << "doesn't work for for extra = " << container_.extra();
        std::cerr << std::endl;
        std::terminate();
    }
    int direction2, direction3;
    FindOtherDirections(direction, &direction2, &direction3);
    
    GridIndex index;
    gridType currentGridType = GridType();
    if (currentGridType > 0) {
        index.SetShift(true, currentGridType);
    }
    int destLeft = rank - 1;
    int destRight = rank + 1;
    {
        Bound left = GetBound(direction, true);
        if (rank == 0) {
            destLeft = left.type == BoundCyclic ? size - 1 : MPI_PROC_NULL;
        }
        if (rank == size - 1) {
            destRight = left.type == BoundCyclic ? 0 : MPI_PROC_NULL;
        }
    }
    
    const int Nbuf = 4;
    BoundLayer leftLayer(Nbuf, 1);
    BoundLayer rightLayer(Nbuf, 1);
    index.SetI(1, direction);
    index.SetI(-1, direction2);
    index.SetI(-1, direction3);                        // 1, -1, -1
    //left
    leftLayer.SetValue(0, 0, this->operator()(index));
    index.SetI(grid.N_loc(direction2), direction2);    // 1, N2, -1
    leftLayer.SetValue(1, 0, this->operator()(index));
    index.SetI(grid.N_loc(direction3), direction3);    // 1, N2, N3
    leftLayer.SetValue(2, 0, this->operator()(index));
    index.SetI(-1, direction2);                        // 1, -1, N3
    leftLayer.SetValue(3, 0, this->operator()(index));
    //right
    index.SetI(grid.N_loc(direction) - 2, direction);  // N1 - 2, -1, N3
    rightLayer.SetValue(3, 0, this->operator()(index));
    index.SetI(grid.N_loc(direction2), direction2);    // N1 - 2, N2, N3
    rightLayer.SetValue(2, 0, this->operator()(index));
    index.SetI(-1, direction3);                        // N1 - 2, N2, -1
    rightLayer.SetValue(1, 0, this->operator()(index));
    index.SetI(-1, direction2);                        // N1 - 2, -1, -1
    rightLayer.SetValue(0, 0, this->operator()(index));
    
    MPI_Status lStatus, rStatus;
    if (rank % 2 == 0) {
        MPI_Sendrecv_replace(leftLayer.GetContainer(), Nbuf, MPI_DOUBLE,
                             destLeft, kGFTagLeftInner,
                             destLeft, kGFTagRightInner,
                             comm, &lStatus);
        MPI_Sendrecv_replace(rightLayer.GetContainer(), Nbuf, MPI_DOUBLE,
                             destRight, kGFTagRightInner,
                             destRight, kGFTagLeftInner,
                             comm, &rStatus);
    } else {
        MPI_Sendrecv_replace(rightLayer.GetContainer(), Nbuf, MPI_DOUBLE,
                             destRight, kGFTagRightInner,
                             destRight, kGFTagLeftInner,
                             comm, &rStatus);
        MPI_Sendrecv_replace(leftLayer.GetContainer(), Nbuf, MPI_DOUBLE,
                             destLeft, kGFTagLeftInner,
                             destLeft, kGFTagRightInner,
                             comm, &lStatus);
    }
    
    index.SetI(-1, direction);
    index.SetI(-1, direction2);
    index.SetI(-1, direction3);                        // -1, -1, -1
    //left
    this->operator[](index) = leftLayer.Value(0, 0);
    index.SetI(grid.N_loc(direction2), direction2);    // -1, N2, -1
    this->operator[](index) = leftLayer.Value(1, 0);
    index.SetI(grid.N_loc(direction3), direction3);    // -1, N2, N3
    this->operator[](index) = leftLayer.Value(2, 0);
    index.SetI(-1, direction2);                        // -1, -1, N3
    this->operator[](index) = leftLayer.Value(3, 0);
    //right
    index.SetI(grid.N_loc(direction), direction);      // N1, -1, N3
    this->operator[](index) = rightLayer.Value(3, 0);
    index.SetI(grid.N_loc(direction2), direction2);    // N1, N2, N3
    this->operator[](index) = rightLayer.Value(2, 0);
    index.SetI(-1, direction3);                        // N1, N2, -1
    this->operator[](index) = rightLayer.Value(1, 0);
    index.SetI(-1, direction2);                        // N1, -1, -1
    this->operator[](index) = rightLayer.Value(0, 0);
    
    MPI_Barrier(comm);
}
#endif

GridFunctionShared operator-(GridFunctionShared left,
                             GridFunctionShared right) {
    return GridFunctionShared(new ReduceFunction(left, right));
}
GridFunctionShared operator+(GridFunctionShared left,
                             GridFunctionShared right) {
    return GridFunctionShared(new SumFunction(left, right));
}
GridFunctionShared operator*(GridFunctionShared left,
                             GridFunctionShared right) {
    return GridFunctionShared(new ProdFunction(left, right));
}

ConstantFShared CF(double value) {
    return ConstantFShared(new ConstantFunction(value));
}

GridFunctionShared DerF(GridFunctionShared func, char c) {
    int dir = 0;
    switch (c) {
        case 'x':
        case 'X':
            dir = 1;
            break;
        case 'y':
        case 'Y':
            dir = 2;
            break;
        case 'z':
        case 'Z':
            dir = 3;
            break;
        default: {
            std::cerr << "DerF: undefined char " << c << std::endl;
            std::terminate();
        }
            break;
    }
    return GridFunctionShared(new Derivative(func, dir));
}


GridFunctionShared DerFD(GridFunctionShared func, int dir) {
    return GridFunctionShared(new Derivative(func, dir));
}
