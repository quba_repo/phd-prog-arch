
#ifndef reaper_h
#define reaper_h

#include "index.h"

struct Reaper {
    Reaper(int N1_, int N2_, int N3_, Index rk_, Index pIndex_)
    : N1(N1_), N2(N2_), N3(N3_), rankIndex(rk_), pIndex(pIndex_) {
        xshift.shiftX = true;
        yshift.shiftY = true;
        zshift.shiftZ = true;
    }
    const int N1;
    const int N2;
    const int N3;
    GridIndex normal;
    GridIndex xshift;
    GridIndex yshift;
    GridIndex zshift;
    Index rankIndex;
    Index pIndex;
};

#define FOR_IN_interior_MACRO(reaper, body) { \
int i_0 = reaper.rankIndex.i > 0 ? 0 : 1;\
int j_0 = reaper.rankIndex.j > 0 ? 0 : 1;\
int k_0 = reaper.rankIndex.k > 0 ? 0 : 1;\
int i_N = reaper.N1;\
int j_N = reaper.N2;\
int k_N = reaper.N3;\
i_N -= reaper.rankIndex.i < (reaper.pIndex.i - 1) ? 0 : 1;\
j_N -= reaper.rankIndex.j < (reaper.pIndex.j - 1) ? 0 : 1;\
k_N -= reaper.rankIndex.k < (reaper.pIndex.k - 1) ? 0 : 1;\
for (int i = i_0; i < i_N; ++i) { \
    reaper.normal.i = i; \
    reaper.xshift.i = i; \
    reaper.yshift.i = i; \
    reaper.zshift.i = i; \
    for (int j = j_0; j < j_N; ++j) { \
        reaper.normal.j = j; \
        reaper.xshift.j = j; \
        reaper.yshift.j = j; \
        reaper.zshift.j = j; \
            for (int k = k_0; k < k_N; ++k) { \
            reaper.normal.k = k; \
            reaper.xshift.k = k; \
            reaper.yshift.k = k; \
            reaper.zshift.k = k; \
            body \
        } \
    } \
} \
}

#define FOR_IN_all_MACRO(reaper, body) { \
int i_0 = 0;\
int j_0 = 0;\
int k_0 = 0;\
int i_N = reaper.N1;\
int j_N = reaper.N2;\
int k_N = reaper.N3;\
for (int i = i_0; i < i_N; ++i) { \
    reaper.normal.i = i; \
    reaper.xshift.i = i; \
    reaper.yshift.i = i; \
    reaper.zshift.i = i; \
    for (int j = j_0; j < j_N; ++j) { \
        reaper.normal.j = j; \
        reaper.xshift.j = j; \
        reaper.yshift.j = j; \
        reaper.zshift.j = j; \
        for (int k = k_0; k < k_N; ++k) { \
            reaper.normal.k = k; \
            reaper.xshift.k = k; \
            reaper.yshift.k = k; \
            reaper.zshift.k = k; \
            body \
        } \
    } \
} \
}

#endif /* reaper_h */
