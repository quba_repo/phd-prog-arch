
#ifndef macroses_h
#define macroses_h

#ifdef PARALLEL_MODE

#ifndef kTagsMPI
    #define kGFTagBounds 121
    #define kGFTagLeftInner 141
    #define kGFTagRightInner 142
    #define kGFTagAddition 666
    #define kFHTagQueue 10
#endif /* kTagBounds */

#endif /* PARALLEL_MODE */

#endif /* macroses_h */
