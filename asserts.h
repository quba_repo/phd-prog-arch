#ifndef asserts_h
#define asserts_h

#define ASSERT_EQ(expected, actual) { \
auto __expected = expected; \
auto __actual = actual; \
if (__expected != __actual) { \
std::cerr << __FILE__ << ":" << __LINE__ << ": Assertion error" << std::endl \
<< "\texpected: " << __expected << " (= " << #expected << ")" << std::endl \
<< "\tgot: " << __actual << " (= " << #actual << ")" << std::endl; \
} \
}

#define ASSERT_DOBL_EQ(expected, actual) { \
auto __expected = expected; \
auto __actual = actual; \
double eps = 0.00000001; \
if (fabs(__expected - __actual) > eps) { \
std::cerr << __FILE__ << ":" << __LINE__ << ": Assertion error" << std::endl \
<< "\texpected: " << __expected << " (= " << #expected << ")" << std::endl \
<< "\tgot: " << __actual << " (= " << #actual << ")" << std::endl; \
} \
}

#endif /* asserts_h */
