//
//  grid_function.h
//  partest
//
//  Created by Антон Кудряшов on 14/12/16.
//  Copyright © 2016 Антон Кудряшов. All rights reserved.
//

#ifndef grid_function_h
#define grid_function_h

#include "macroses.h"

#include "array_container.h"
#include "data_source.h"
#include "grid.h"
#include "index.h"
#include "layer.h"

#ifdef PARALLEL_MODE
#include "thread.h"
#endif

void FindOtherDirections(const int direction, int* direction2, int* direction3);

typedef std::shared_ptr<Grid> GridShared;

class GridFunction;
typedef std::shared_ptr<GridFunction> GridFunctionShared;
// Base Class
class GridFunction {
    friend class UnaryFunction;
    friend class BinaryFunction;
 public:
    GridFunction(GridShared grid__) :
    grid(*grid__.get()),
    grid_(grid__)
    {};
    virtual const double operator()(const GridIndex& index) const = 0;
    virtual gridType GridType() const {
        return grid.GridType();
    }
    friend GridFunctionShared operator-(GridFunctionShared left,
                                        GridFunctionShared right);
    friend GridFunctionShared operator+(GridFunctionShared left,
                                        GridFunctionShared right);
    friend GridFunctionShared operator*(GridFunctionShared left,
                                        GridFunctionShared right);
    const Grid& grid;
    const GridShared GetGridShared() {
        return grid_;
    }
 protected:
    GridShared grid_;
};

// Unary function
class UnaryFunction : public GridFunction {
 public:
    UnaryFunction(GridFunctionShared func)
    : GridFunction(func->grid_),
    func_(func) {};
    virtual const double operator()(const GridIndex& index) const = 0;
 protected:
    GridFunctionShared func_;
};

// Binary function
class BinaryFunction : public GridFunction {
 public:
    BinaryFunction(GridFunctionShared left, GridFunctionShared right)
    : GridFunction(left->grid_ != nullptr ? left->grid_ : right->grid_),
    left_(left), right_(right) {};
    virtual const double operator()(const GridIndex& index) const = 0;
 protected:
    GridFunctionShared left_;
    GridFunctionShared right_;
};

// Arrayed function
class ArrayedFunc;
typedef std::shared_ptr<ArrayedFunc> ArrayedFuncShared;

class ArrayedFunc : public GridFunction {
 public:
    ArrayedFunc(GridShared grid, const DataSource& dataSource,
                char boundChar)
    : GridFunction(grid),
    container_(ArrayContainer(grid->N_loc(1),
                              grid->N_loc(2),
                              grid->N_loc(3),
                              1)),
    layers_(AdditionalLayers(dataSource.Px,
                             dataSource.Py,
                             dataSource.Pz,
                             grid->N_loc(1),
                             grid->N_loc(2),
                             grid->N_loc(3),
                             grid->RankOffset())) {
        bounds_ = dataSource.Bounds(boundChar);
    };
    const double operator()(const GridIndex& index) const {
        gridType currentGrid = GridType();
        
        if (index.rankP > 0) {
            bool shifted = index.Shift(index.directionP);
            BoundLayer* layer = AdditionalLayer(index.rankP, index.directionP);
            int dir2,dir3;
            FindOtherDirections(index.directionP, &dir2, &dir3);
            int j = index.GetI(dir2);
            int k = index.GetI(dir3);
            if (shifted && currentGrid != index.directionP) {
                return (layer->Value(j, k) + layer->Value(j, k + grid_->N_loc(dir3)))/2.;
            }
            return layer->Value(j, k + grid_->N_loc(dir3));
        }
        
        for (int i=1; i<=3; ++i) {
            bool shifted = index.Shift(i);
            if (shifted && currentGrid != i) {
                GridIndex indexL = index;
                GridIndex indexR = index;
                grid.MovePrevShift(&indexL, i);
                grid.MoveNextShift(&indexR, i);
                return (this->operator()(indexL) + this->operator()(indexR))/2.;
            }
        }
        return container_[index];
    };
    BoundLayer* AdditionalLayer(int rank, int direction) const {
        return layers_.Layer(rank, direction);
    };
    double& operator[](const GridIndex& index) {
        gridType currentGrid = GridType();
        for (int i=1; i<=3; ++i) {
            bool shifted = index.Shift(i);
            if (shifted && currentGrid != i) {
                std::cerr << "trying to fill out of data value" << std::endl;
                std::terminate();
            }
        }
        return container_[index];
    };
    template <class _Input>
    void FillFrom(_Input input) {
        GridIndex index;
        gridType currentGridType = GridType();
        if (currentGridType > 0) {
            index.SetShift(true, currentGridType);
        }
        int extra = (int)container_.extra();
        for (int i=(-extra); i < grid.N_loc(1) + extra; ++i) {
            for (int j=(-extra); j < grid.N_loc(2) + extra; ++j) {
                for (int k=(-extra); k < grid.N_loc(3) + extra; ++k) {
                    index.i = i;
                    index.j = j;
                    index.k = k;
                    Location loc = grid.GetLocation(index);
                    this->operator[](index) = input(loc.x, loc.y, loc.z);
                }
            }
        }
    }
    void FillFrom(double (DataSource::*func)(double, double, double) const,
                  const DataSource& dataSource) {
        FillFrom([&](double x, double y, double z){
            return (dataSource.*func)(x, y, z);
        });
    }
    Bound GetBound(int direction, bool left) const {
        switch (direction) {
            case 1:
                return left ? bounds_.Lx : bounds_.Rx;
            case 2:
                return left ? bounds_.Ly : bounds_.Ry;
            case 3:
                return left ? bounds_.Lz : bounds_.Rz;
            default:
                break;
        }
        std::cerr << "Bound is not found for grid in " << direction << std::endl;
        std::terminate();
    }
#ifdef PARALLEL_MODE
    void UpdateBounds(const Control::Thread& thread);
    void UpdateOuter(int direction, const Control::Thread& thread);
    void UpdateSides(int direction, const Control::Thread& thread);
    void UpdateAddition(int direction, const Control::Thread& thread);
    void UpdateEdges(int direction, const Control::Thread& thread);
    void UpdateCorners(int direction, const Control::Thread& thread);
#endif

 private:
    BoundTuple bounds_;
    ArrayContainer container_;
    AdditionalLayers layers_;
};

//Closure functions
class ConstantFunction;
typedef std::shared_ptr<ConstantFunction> ConstantFShared;

class ConstantFunction : public GridFunction {
 public:
    ConstantFunction(double value) : GridFunction(nullptr), value_(value) {};
    const double operator()(const GridIndex& index) const {
        return value_;
    }
    void SetValue(double value) {
        value_ = value;
    }
 private:
    double value_;
};
ConstantFShared CF(double value);

class ReduceFunction : public BinaryFunction {
 public:
    ReduceFunction(GridFunctionShared left, GridFunctionShared right)
    : BinaryFunction(left, right) {}
    const double operator()(const GridIndex& index) const {
        return left_->operator()(index) - right_->operator()(index);
    }
};

class SumFunction : public BinaryFunction {
 public:
    SumFunction(GridFunctionShared left, GridFunctionShared right)
    : BinaryFunction(left, right) {}
    const double operator()(const GridIndex& index) const {
        return left_->operator()(index) + right_->operator()(index);
    }
};

class ProdFunction : public BinaryFunction {
 public:
    ProdFunction(GridFunctionShared left, GridFunctionShared right)
    : BinaryFunction(left, right) {}
    const double operator()(const GridIndex& index) const {
        return left_->operator()(index) * right_->operator()(index);
    }
};

//Derivative function
class Derivative : public UnaryFunction {
 public:
    Derivative(GridFunctionShared func, int direction)
    : UnaryFunction(func) , direction_(direction) {}
    const double operator()(const GridIndex& index) const {
        GridIndex indexL = index;
        GridIndex indexR = index;
        grid.MovePrevShift(&indexL, direction_);
        grid.MoveNextShift(&indexR, direction_);
        return (func_->operator()(indexR) - func_->operator()(indexL)
                )/grid.H(direction_);
    }
 private:
    int direction_;
};

GridFunctionShared DerF(GridFunctionShared func, char c);
GridFunctionShared DerFD(GridFunctionShared func, int dir);

class CharacteristicFunction;
typedef std::shared_ptr<CharacteristicFunction> CharacteristicFShared;
//Characteristic function
class CharacteristicFunction : public GridFunction {
 public:
    CharacteristicFunction(int direction, GridShared gridPtr,
                           int index = 0)
    : GridFunction(gridPtr), direction_(direction), index_(index) {
        shift_ = direction == gridPtr->GridType();
    };
    const double operator()(const GridIndex& index) const {
        bool shift = index.Shift(direction_);
        int ix = index.GetI(direction_);
        if (shift == shift_) {
            return index_ == ix;
        }
        else if (index_ == ix
                 || (ix == index_ + 1 && shift)
                 || (index_ == ix + 1 && shift_)) {
            return 0.5;
        }
        return 0;
    }
    void SetIndex(int index) {
        index_ = index;
    }
private:
    int direction_;
    int index_;
    bool shift_;
};

#endif /* grid_function_h */
