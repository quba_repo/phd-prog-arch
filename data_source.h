#ifndef data_source_h
#define data_source_h

#pragma once
#include <memory>
#include <vector>

#include "location.h"
#include "interval.h"
#include "bound.h"
#include "iteration_type.h"

// Data Source object
//
struct DataSource {
    // processors
    int Px;
    int Py;
    int Pz;
    // cells
    int Nx;
    int Ny;
    int Nz;
    int nlocx;
    int nlocy;
    int nlocz;
    double hx;
    double hy;
    double hz;
    // cube domain
    Interval Ix;
    Interval Iy;
    Interval Iz;
    // bound conditions
    BoundTuple boundsU;
    BoundTuple boundsV;
    BoundTuple boundsW;
    BoundTuple boundsP;
    
    BoundTuple Bounds(char c) const {
        switch (c) {
            case 'u':
                return boundsU;
            case 'v':
                return boundsV;
            case 'w':
                return boundsW;
            case 'p':
                return boundsP;
            case '0': {
                BoundTuple t;
                t.Lx = Bound(boundsP.Lx.type);
                t.Rx = Bound(boundsP.Rx.type);
                t.Ly = Bound(boundsP.Ly.type);
                t.Ry = Bound(boundsP.Ry.type);
                t.Lz = Bound(boundsP.Lz.type);
                t.Rz = Bound(boundsP.Rz.type);
                return t;
            }
            case '1': {
                BoundTuple t;
                t.Lx = Bound(boundsU.Lx.type);
                t.Rx = Bound(boundsU.Rx.type);
                t.Ly = Bound(boundsU.Ly.type);
                t.Ry = Bound(boundsU.Ry.type);
                t.Lz = Bound(boundsU.Lz.type);
                t.Rz = Bound(boundsU.Rz.type);
                return t;
            }
            case '2': {
                BoundTuple t;
                t.Lx = Bound(boundsV.Lx.type);
                t.Rx = Bound(boundsV.Rx.type);
                t.Ly = Bound(boundsV.Ly.type);
                t.Ry = Bound(boundsV.Ry.type);
                t.Lz = Bound(boundsV.Lz.type);
                t.Rz = Bound(boundsV.Rz.type);
                return t;
            }
            case '3': {
                BoundTuple t;
                t.Lx = Bound(boundsW.Lx.type);
                t.Rx = Bound(boundsW.Rx.type);
                t.Ly = Bound(boundsW.Ly.type);
                t.Ry = Bound(boundsW.Ry.type);
                t.Lz = Bound(boundsW.Lz.type);
                t.Rz = Bound(boundsW.Rz.type);
                return t;
            }
            default:
                break;
        }
        return BoundTuple();
    }
    // scheme parameters
    // alpha
    double alpha;
    double alphaPuasson;
    // tau
    double tau;
    double tauPuasson;
    // max time steps
    int maxSteps;
    int maxStepsPuasson;
    // precision
    int epsilon;
    int epsilonPuasson;
    // iteration type
    iterationType itersType;
    
    // initial functions
    double p_0(double x, double y, double z) const;
    double u_0(double x, double y, double z) const;
    double v_0(double x, double y, double z) const;
    double w_0(double x, double y, double z) const;
    
    // description
    void PrintDescription() const;
    
    // parallel filling
    void RankOffsetUpdate(Index rankOffset);
};

typedef std::unique_ptr<DataSource> DataSourcePTR;

namespace DataSourceFactory {
    DataSourcePTR GetDataSource(int Px, int Py, int Pz);
}

#endif /* data_source_h */
