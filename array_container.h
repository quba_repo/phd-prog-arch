//
//  3d_array.h
//  partest
//
//  Created by Антон Кудряшов on 14/12/16.
//  Copyright © 2016 Антон Кудряшов. All rights reserved.
//

#ifndef _thd_array_h
#define _thd_array_h

#pragma once
#include <vector>

#include "index.h"

class ArrayContainer {
 public:
    ArrayContainer(size_t Nx, size_t Ny, size_t Nz, size_t ExtraLayers = 0) {
        Update_(Nx, Ny, Nz, ExtraLayers);
        v_ = std::vector<double>(Nx_*Ny_*Nz_, 0);
    }
    size_t size(int direction) {
        switch (direction) {
            case 1:
                return Nx_;
            case 2:
                return Ny_;
            case 3:
                return Nz_;
            default:
                break;
        }
        std::cerr << "size is not found for array" << std::endl;
        std::terminate();
        return -1111;
    }
    size_t extra() const {
        return Extra_;
    }
    // resize with clearing
    void resize(size_t Nx, size_t Ny, size_t Nz, size_t ExtraLayers = 0) {
        Update_(Nx, Ny, Nz, ExtraLayers);
        size_t Count = Nx_*Ny_*Nz_;
        if (Count > 0) {
            v_.resize(Count);
            for (int i=0; i < (int)Nx_; ++i) {
                for (int j=0; j < (int)Ny_; ++j) {
                    for (int k=0; k < (int)Nz_; ++k) {
                        int index = (int)Nx_*(int)Ny_*k + (int)Nx_*j + i;
                        v_[index] = 0;
                    }
                }
            }
        }
    }
    double& operator[](const Index& index) {
        CheckIndex_(index);
        return v_[Nx_*Ny_*(index.k + Extra_) + Nx_*(index.j + Extra_) + index.i + Extra_];
    };
    const double& operator[](const Index& index) const {
        CheckIndex_(index);
        return v_[Nx_*Ny_*(index.k + Extra_) + Nx_*(index.j + Extra_) + index.i + Extra_];
    }
 private:
    std::vector<double> v_;
    size_t Nx_;
    size_t Ny_;
    size_t Nz_;
    size_t Extra_;
    
    void Update_(size_t Nx, size_t Ny, size_t Nz, size_t ExtraLayers) {
        bool has = Nx*Ny*Nz > 0;
        Nx_ = has ? Nx + 2*ExtraLayers : 0;
        Ny_ = has ? Ny + 2*ExtraLayers : 0;
        Nz_ = has ? Nz + 2*ExtraLayers : 0;
        Extra_ = has ? ExtraLayers : 0;
    }
    void CheckIndex_(const Index& index) const {
        if (index.i + (int)Extra_ < 0 || index.i + (int)Extra_ >= (int)Nx_) {
            std::cerr << "ArrayC:out of range i:" << index.Description();
            std::cerr << " with " << Nx_ << " " << Ny_ << " " << Nz_ << std::endl;
            std::terminate();
        }
        if (index.j + (int)Extra_ < 0 || index.j + (int)Extra_ >= (int)Ny_) {
            std::cerr << "ArrayC:out of range j:" << index.Description();
            std::cerr << " with " << Nx_ << " " << Ny_ << " " << Nz_ << std::endl;
            std::terminate();
        }
        if (index.k + (int)Extra_ < 0 || index.k + (int)Extra_ >= (int)Nz_) {
            std::cerr << "ArrayC:out of range k:" << index.Description();
            std::cerr << " with " << Nx_ << " " << Ny_ << " " << Nz_ << std::endl;
            std::terminate();
        }
    }
};

#endif /* _thd_array_h */
