//
//  grid.h
//  partest
//
//  Created by Антон Кудряшов on 14/12/16.
//  Copyright © 2016 Антон Кудряшов. All rights reserved.
//

#ifndef grid_h
#define grid_h

#include "data_source.h"
#include "grid_type.h"
#include "location.h"

class Grid {
 public:
    Grid(gridType type, const DataSource& dataSource,
         char boundChar, Index rankOffset = Index())
    : type_(type) {
        int Nx = dataSource.Nx;
        int Ny = dataSource.Ny;
        int Nz = dataSource.Nz;
        bool has = Nx*Ny*Nz > 0;
        Nx_ = has ? Nx : 0;
        Ny_ = has ? Ny : 0;
        Nz_ = has ? Nz : 0;
        nx_ = has ? dataSource.nlocx : 0;
        ny_ = has ? dataSource.nlocy : 0;
        nz_ = has ? dataSource.nlocz : 0;
        Hx_ = has && Nx > 1 ? dataSource.hx : 0;
        Hy_ = has && Ny > 1 ? dataSource.hy : 0;
        Hz_ = has && Nz > 1 ? dataSource.hz : 0;
        Ix_ = dataSource.Ix;
        Iy_ = dataSource.Iy;
        Iz_ = dataSource.Iz;
        if (!has) {
            std::cerr << "trying to construct grid with empty cells" << std::endl;
            std::terminate();
        }
        BoundTuple bounds = dataSource.Bounds(boundChar);
        // cyclic feature needs to get value from ending and at begining, and
        // ofcourse this will be work, if in each direction we have only 1 proc.
        // otherwise, we need to store extra values in additional space with
        // prev/next indeces
        cyclicX_ = dataSource.Px == 1 && bounds.Lx.type == BoundCyclic;
        cyclicY_ = dataSource.Py == 1 && bounds.Ly.type == BoundCyclic;
        cyclicZ_ = dataSource.Pz == 1 && bounds.Lz.type == BoundCyclic;
        // rank offset - feature is needed for correct location 
        offset_ = Index(rankOffset.i*(nx_ - 1), rankOffset.j*(ny_ - 1), rankOffset.k*(nz_ - 1));
        rankOffset_ = rankOffset;
    }
    // global N
    int N(int direction) const {
        switch (direction) {
            case 1:
                return Nx_;
            case 2:
                return Ny_;
            case 3:
                return Nz_;
            default:
                break;
        }
        std::cerr << "size is not found for grid in " << direction << std::endl;
        std::terminate();
        return -1111;
    }
    int N_loc(int direction) const {
        switch (direction) {
            case 1:
                return nx_;
            case 2:
                return ny_;
            case 3:
                return nz_;
            default:
                break;
        }
        std::cerr << "loc size is not found for grid in " << direction << std::endl;
        std::terminate();
        return -1111;
    }
    double H(int direction) const {
        switch (direction) {
            case 1:
                return Hx_;
            case 2:
                return Hy_;
            case 3:
                return Hz_;
            default:
                break;
        }
        std::cerr << "H is not found for grid in " << direction << std::endl;
        std::terminate();
        return -1111;
    }
    void MovePrev(Index* index, int direction) const {
        switch (direction) {
            case 1: {
                if (index->i == 0 && cyclicX_) {
                    index->i = Nx_ - 1;
                }
                index->i--;
                return;
            }
            case 2: {
                if (index->j == 0 && cyclicY_) {
                    index->j = Ny_ - 1;
                }
                index->j--;
                return;
            }
            case 3: {
                if (index->k == 0 && cyclicZ_) {
                    index->k = Nz_ - 1;
                }
                index->k--;
                return;
            }
            default:
                break;
        }
        std::cerr << "prev move in direction " << direction;
        std::cerr << " is not found for index " << index->Description();
        std::cerr << " gridtype " << GridType() << std::endl;
        std::terminate();
    }
    void MoveNext(Index* index, int direction) const {
        switch (direction) {
            case 1: {
                if (index->i == Nx_ - 1 && cyclicX_) {
                    index->i = 0;
                }
                index->i++;
                return;
            }
            case 2: {
                if (index->j == Ny_ - 1 && cyclicY_) {
                    index->j = 0;
                }
                index->j++;
                return;
            }
            case 3: {
                if (index->k == Nz_ - 1 && cyclicZ_) {
                    index->k = 0;
                }
                index->k++;
                return;
            }
            default:
                break;
        }
        std::cerr << "next move in direction " << direction;
        std::cerr << " is not found for index " << index->Description();
        std::cerr << " gridtype " << GridType() << std::endl;
        std::terminate();
    }
    void MovePrevShift(GridIndex* index, int direction) const {
        bool shifted = index->Shift(direction);
        index->SetShift(!shifted, direction);
        if (shifted) {
            MovePrev(index, direction);
        }
    }
    void MoveNextShift(GridIndex* index, int direction) const {
        bool shifted = index->Shift(direction);
        index->SetShift(!shifted, direction);
        if (!shifted) {
            MoveNext(index, direction);
        }
    }
    gridType GridType() const {
        return type_;
    }
    Location GetLocation(const GridIndex& index) const {
        Location loc;
        loc.x = Ix_.a + (offset_.i + index.i)*Hx_;
        loc.y = Iy_.a + (offset_.j + index.j)*Hy_;
        loc.z = Iz_.a + (offset_.k + index.k)*Hz_;
        if (index.shiftX) {
            loc.x -= Hx_/2.;
        }
        if (index.shiftY) {
            loc.y -= Hy_/2.;
        }
        if (index.shiftZ) {
            loc.z -= Hz_/2.;
        }
        return loc;
    }
    Index RankOffset() const {
        return rankOffset_;
    }
 private:
    gridType type_;
    int Nx_;
    int Ny_;
    int Nz_;
    // per processor
    int nx_;
    int ny_;
    int nz_;
    double Hx_;
    double Hy_;
    double Hz_;
    bool cyclicX_;
    bool cyclicY_;
    bool cyclicZ_;
    Interval Ix_;
    Interval Iy_;
    Interval Iz_;
    Index offset_;
    Index rankOffset_;
};

#endif /* grid_h */
