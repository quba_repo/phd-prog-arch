#include <iostream>
#include <cassert>
#include <vector>
#include <memory>
#include <sstream>

#include "macroses.h"

#include "asserts.h"
#include "data_source.h"
#include "slae.h"
#include "array_container.h"
#include "grid_function.h"

void testTMS() {
    std::vector<double> l, c, r, d;
    for (int i = 0; i < 4; i++) {
        l.push_back(1 + 10*i);
        c.push_back(2 + 10*i);
        r.push_back(3 + 10*i);
    }
    d.resize(4);
    SLAE::TrigiagonalEquations eq(l, c, r, d);
    std::vector<std::string> answ = {
        "2 3 0 0",
        "11 12 13 0",
        "0 21 22 23",
        "0 0 31 32"
    };
    for (int i = 0; i < 4; i++) {
        std::stringstream ss;
        for (int j = 0; j < 4; j++) {
            ss << eq.A(i, j);
            if (j < 3) {
                ss << " ";
            }
        }
        std::string s2 = ss.str();
        std::string s = answ[i];
        ASSERT_EQ(s, s2);
    }
}

void testSolver() {
    std::vector<double> l, c, r, d;
    
    std::vector<double> x;
    ASSERT_EQ(0, (int)x.size());
    
    for (int i = 0; i < 3; i++) {
        l.push_back(1);
        c.push_back(2 + i);
        r.push_back(1 + 2*i);
    }
    d.resize(3);
    
    SLAE::TridiagonalMatrixSolver solver2(std::unique_ptr<SLAE::TrigiagonalEquations>(new SLAE::TrigiagonalEquations(l, c, r, d)));
    solver2.Solve(&x);
    for (int i = 0; i< 3; i++) {
        ASSERT_DOBL_EQ(0, x[i]);
        d[i] = 1;
    }
    
    SLAE::TridiagonalMatrixSolver solver3(std::unique_ptr<SLAE::TrigiagonalEquations>(new SLAE::TrigiagonalEquations(l, c, r, d)));
    solver3.Solve(&x);
    std::vector<double> res = {
        4./7., -1./7., 2./7.
    };
    for (int i = 0; i< 3; i++) {
        ASSERT_DOBL_EQ(res[i], x[i]);
        l[i] = 0;
        c[i] = 1;
        r[i] = 0;
        d[i] = 2*i;
    }
    
    SLAE::TridiagonalMatrixSolver solver4(std::unique_ptr<SLAE::TrigiagonalEquations>(new SLAE::TrigiagonalEquations(l, c, r, d)));
    solver4.Solve(&x);
    for (int i = 0; i< 3; i++) {
        ASSERT_DOBL_EQ(d[i], x[i]);
    }
}

void stressTest() {
    for (int N = 3; N < 50; N++) {
        std::vector<double> l, c, r, d;
        std::vector<double> x;
        for (int i = 0; i < N; i++) {
            l.push_back(1);
            c.push_back(2);
            r.push_back(1);
            double v = 2*i;
            if (i > 0) {
                v += i-1;
            }
            if (i < N - 1) {
                v += i+1;
            }
            d.push_back(v);
        }
        SLAE::TridiagonalMatrixSolver solver(std::unique_ptr<SLAE::TrigiagonalEquations>(new SLAE::TrigiagonalEquations(l, c, r, d)));
        solver.Solve(&x);
        ASSERT_EQ(N, (int)x.size());
        for (int i = 0; i < N; i++) {
            ASSERT_DOBL_EQ(i, x[i]);
        }
    }
    int N = 3;
    std::vector<double> l, c, r, d;
    std::vector<double> x, res;
    for (int i = 0; i < N; i++) {
        res.push_back(i + 1);
        if (i == N - 1) {
            res[i] = res[0];
        }
    }
    for (int i = 0; i < N; i++) {
        l.push_back(1);
        c.push_back(2);
        r.push_back(1);
        double v = 2*res[i];
        if (i > 0) {
            v += res[i-1];
        }
        if (i < N - 1) {
            v += res[i+1];
        }
        if (i == 0) {
            v += res[N-1];
        }
        if (i == N-1) {
            v += res[0];
        }
        d.push_back(v);
    }
    SLAE::TridiagonalMatrixSolver solver(std::unique_ptr<SLAE::TrigiagonalEquations>(new SLAE::TrigiagonalEquations(l, c, r, d, true)));
    solver.Solve(&x);
    ASSERT_EQ(N, (int)x.size());
    for (int i = 0; i < N; i++) {
        ASSERT_DOBL_EQ(res[i], x[i]);
    }
}

void testCyclicTMS() {
    std::vector<double> l, c, r, d;
    for (int i = 0; i < 4; i++) {
        l.push_back(1 + 10*i);
        c.push_back(2 + 10*i);
        r.push_back(3 + 10*i);
    }
    d.resize(4);
    SLAE::TrigiagonalEquations eq(l, c, r, d, true);
    std::vector<std::string> answ = {
        "2 3 0 1",
        "11 12 13 0",
        "0 21 22 23",
        "33 0 31 32"
    };
    for (int i = 0; i < 4; i++) {
        std::stringstream ss;
        for (int j = 0; j < 4; j++) {
            ss << eq.A(i, j);
            if (j < 3) {
                ss << " ";
            }
        }
        std::string s2 = ss.str();
        std::string s = answ[i];
        ASSERT_EQ(s, s2);
    }
}

void test3dArray() {
    ArrayContainer array = ArrayContainer(0, 0, 0);
    ASSERT_EQ(0, (int)array.size(1));
    ASSERT_EQ(0, (int)array.size(2));
    ASSERT_EQ(0, (int)array.size(3));
    
    array.resize(0, 10, 0);
    ASSERT_EQ(0, (int)array.size(1));
    ASSERT_EQ(0, (int)array.size(2));
    ASSERT_EQ(0, (int)array.size(3));
    
    array.resize(0, 10, 40);
    ASSERT_EQ(0, (int)array.size(1));
    ASSERT_EQ(0, (int)array.size(2));
    ASSERT_EQ(0, (int)array.size(3));
    
    int N1 = 10;
    int N2 = 15;
    int N3 = 25;
    array.resize(N1, N2, N3);
    ASSERT_EQ(N1, (int)array.size(1));
    ASSERT_EQ(N2, (int)array.size(2));
    ASSERT_EQ(N3, (int)array.size(3));
    Index indx = {0, 0, 0};
    for (int i=0; i < N1; ++i) {
        for (int j=0; j < N2; ++j) {
            for (int k=0; k < N3; ++k) {
                indx.i = i;
                indx.j = j;
                indx.k = k;
                array[indx] = N1*N2*k + N1*j + i;
                double value = array[indx];
                ASSERT_DOBL_EQ(N1*N2*k + N1*j + i, value);
            }
        }
    }
}

void testGrids() {
    DataSourcePTR dataptr = DataSourceFactory::GetDataSource(1, 1, 1);
    DataSource* source = dataptr.get();
    
    source->Nx = 10;
    source->Ny = 15;
    source->Nz = 25;
    Grid grid3 = Grid(GridNormal, *source, 'p');
    ASSERT_EQ(source->Nx, grid3.N(1));
    ASSERT_EQ(source->Ny, grid3.N(2));
    ASSERT_EQ(source->Nz, grid3.N(3));
    int N1 = source->Nx;
    int N2 = source->Ny;
    int N3 = source->Nz;
    GridIndex indx;
    for (int i=0; i < N1-1; ++i) {
        indx.j = 0;
        for (int j=0; j < N2-1; ++j) {
            indx.k = 0;
            for (int k=0; k < N3-1; ++k) {
                grid3.MoveNext(&indx, 3);
                ASSERT_EQ(k+1, indx.k);
            }
            grid3.MoveNext(&indx, 2);
            ASSERT_EQ(j+1, indx.j);
        }
        grid3.MoveNext(&indx, 1);
        ASSERT_EQ(i+1, indx.i);
    }
    //coping
    GridIndex index2 = indx;
    ASSERT_EQ(index2.i, indx.i);
    ASSERT_EQ(index2.j, indx.j);
    ASSERT_EQ(index2.k, indx.k);
    ASSERT_EQ(false, indx.shiftX);
    ASSERT_EQ(false, indx.shiftY);
    ASSERT_EQ(false, indx.shiftZ);
    
    for (int i=N1-1; i > 0; --i) {
        indx.j = N2-1;
        for (int j=N2-1; j > 0; --j) {
            indx.k = N3-1;
            for (int k=N3-1; k > 0; --k) {
                grid3.MovePrev(&indx, 3);
                ASSERT_EQ(k-1, indx.k);
            }
            grid3.MovePrev(&indx, 2);
            ASSERT_EQ(j-1, indx.j);
        }
        grid3.MovePrev(&indx, 1);
        ASSERT_EQ(i-1, indx.i);
    }
    
    source->boundsP.Lx = { BoundCyclic, 0 };
    source->boundsP.Rx = { BoundCyclic, 0 };
    source->boundsP.Ly = { BoundCyclic, 0 };
    source->boundsP.Ry = { BoundCyclic, 0 };
    source->boundsP.Lz = { BoundCyclic, 0 };
    source->boundsP.Rz = { BoundCyclic, 0 };
    Grid grid4 = Grid(GridNormal, *source, 'p');
    ASSERT_EQ(N1, grid4.N(1));
    ASSERT_EQ(N2, grid4.N(2));
    ASSERT_EQ(N3, grid4.N(3));
    indx.i = 0;
    for (int i=0; i < N1; ++i) {
        indx.j = 0;
        for (int j=0; j < N2; ++j) {
            indx.k = 0;
            for (int k=0; k < N3; ++k) {
                grid4.MoveNext(&indx, 3);
                ASSERT_EQ(k == N3 - 1 ? 1 : k+1, indx.k);
            }
            grid4.MoveNext(&indx, 2);
            ASSERT_EQ(j == N2 - 1 ? 1 : j+1, indx.j);
        }
        grid4.MoveNext(&indx, 1);
        ASSERT_EQ(i == N1 - 1 ? 1 : i+1, indx.i);
    }
    indx.i = N1 - 1;
    for (int i=N1-1; i >= 0; --i) {
        indx.j = N2-1;
        for (int j=N2-1; j >= 0; --j) {
            indx.k = N3-1;
            for (int k=N3-1; k >= 0; --k) {
                grid4.MovePrev(&indx, 3);
                ASSERT_EQ(k == 0 ? N3 - 2 : k-1, indx.k);
            }
            grid4.MovePrev(&indx, 2);
            ASSERT_EQ(j == 0 ? N2 - 2 : j-1, indx.j);
        }
        grid4.MovePrev(&indx, 1);
        ASSERT_EQ(i == 0 ? N1 - 2 : i-1, indx.i);
    }
    
    GridIndex index3;
    ASSERT_EQ(0, index3.i);
    ASSERT_EQ(0, index3.j);
    ASSERT_EQ(0, index3.k);
    ASSERT_EQ(false, index3.shiftX);
    ASSERT_EQ(false, index3.shiftY);
    ASSERT_EQ(false, index3.shiftZ);
    grid3.MoveNextShift(&index3, 1);
    ASSERT_EQ(1, index3.i);
    ASSERT_EQ(0, index3.j);
    ASSERT_EQ(0, index3.k);
    ASSERT_EQ(true, index3.shiftX);
    ASSERT_EQ(false, index3.shiftY);
    ASSERT_EQ(false, index3.shiftZ);
    grid3.MoveNextShift(&index3, 1);
    ASSERT_EQ(1, index3.i);
    ASSERT_EQ(0, index3.j);
    ASSERT_EQ(0, index3.k);
    ASSERT_EQ(false, index3.shiftX);
    ASSERT_EQ(false, index3.shiftY);
    ASSERT_EQ(false, index3.shiftZ);
    grid3.MovePrev(&index3, 1);
    grid3.MoveNext(&index3, 1);
    grid3.MoveNext(&index3, 1);
    grid3.MovePrevShift(&index3, 2);
    ASSERT_EQ(2, index3.i);
    ASSERT_EQ(0, index3.j);
    ASSERT_EQ(0, index3.k);
    ASSERT_EQ(false, index3.shiftX);
    ASSERT_EQ(true, index3.shiftY);
    ASSERT_EQ(false, index3.shiftZ);
    grid3.MoveNextShift(&index3, 2);
    ASSERT_EQ(2, index3.i);
    ASSERT_EQ(0, index3.j);
    ASSERT_EQ(0, index3.k);
    ASSERT_EQ(false, index3.shiftX);
    ASSERT_EQ(false, index3.shiftY);
    ASSERT_EQ(false, index3.shiftZ);
    grid3.MoveNextShift(&index3, 2);
    ASSERT_EQ(2, index3.i);
    ASSERT_EQ(1, index3.j);
    ASSERT_EQ(0, index3.k);
    ASSERT_EQ(false, index3.shiftX);
    ASSERT_EQ(true, index3.shiftY);
    ASSERT_EQ(false, index3.shiftZ);
}

void testGridFunctions() {
    DataSourcePTR dataptr = DataSourceFactory::GetDataSource(1, 1, 1);
    DataSource* source = dataptr.get();
    int N1 = dataptr->Nx;
    int N2 = dataptr->Ny;
    int N3 = dataptr->Nz;
    
    GridShared grid = GridShared(new Grid(GridNormal, *source, 'p'));
    ArrayedFunc u(grid, *source);
    GridIndex index;
    const double m1 = 100;
    const double m2 = 1000;
    for (int i=0; i < N1; ++i) {
        for (int j=0; j < N2; ++j) {
            for (int k=0; k < N3; ++k) {
                index.i = i;
                index.j = j;
                index.k = k;
                double v = i + m1*j + m2*k;
                u[index] = v;
                ASSERT_EQ(v, u(index));
            }
        }
    }
    index.i = 0;
    index.j = 0;
    index.k = 0;
    index.shiftX = true;
    ASSERT_EQ(true, index.shiftX);
    ASSERT_EQ(false, index.shiftY);
    ASSERT_EQ(false, index.shiftZ);
    GridIndex indexY = index;
    ASSERT_EQ(indexY.i, index.i);
    ASSERT_EQ(indexY.j, index.j);
    ASSERT_EQ(indexY.k, index.k);
    ASSERT_EQ(true, indexY.shiftX);
    ASSERT_EQ(false, indexY.shiftY);
    ASSERT_EQ(false, indexY.shiftZ);
    indexY.shiftY = true;
    ASSERT_EQ(true, indexY.shiftY);
    for (int i=0; i < N1 - 1; ++i) {
        grid->MoveNext(&index, GridShiftedX);
        grid->MoveNext(&indexY, GridShiftedX);
        double v = (i + i + 1.)/2.;
        ASSERT_DOBL_EQ(v, u(index));
        indexY.j = 0;
        for (int j=0; j < N2-1; ++j) {
            grid->MoveNext(&indexY, GridShiftedY);
            double w = m1*(j + j + 1)/2. + v;
            ASSERT_DOBL_EQ(w, u(indexY));
        }
    }
}

double fx(double x, double y, double z) {
    return x;
}
double fy(double x, double y, double z) {
    return y;
}

void testGridFilling() {
    DataSourcePTR dataptr = DataSourceFactory::GetDataSource(1, 1, 1);
    DataSource* source = dataptr.get();
    int N1 = dataptr->Nx;
    int N2 = dataptr->Ny;
    int N3 = dataptr->Nz;
    GridShared grid = GridShared(new Grid(GridNormal, *source, 'p'));
    ArrayedFunc p(grid, *source);
    p.FillFrom(&DataSource::p_0, *source);
    GridIndex index;
    for (int i=0; i < N1; ++i) {
        for (int j=0; j < N2; ++j) {
            for (int k=0; k < N3; ++k) {
                index.i = i;
                index.j = j;
                index.k = k;
                Location loc = grid->GetLocation(index);
                ASSERT_EQ(source->p_0(loc.x, loc.y, loc.z), p(index));
            }
        }
    }
    GridShared grid2 = GridShared(new Grid(GridShiftedX, *source, 'p'));
    ArrayedFunc u2(grid2, *source);
    u2.FillFrom(&fx);
    index.shiftX = true;
    for (int i=0; i < N1; ++i) {
        for (int j=0; j < N2; ++j) {
            for (int k=0; k < N3; ++k) {
                index.i = i;
                index.j = j;
                index.k = k;
                Location loc = grid->GetLocation(index);
                ASSERT_EQ(fx(loc.x, loc.y, loc.z), u2(index));
            }
        }
    }
    u2.FillFrom(&fy);
    for (int i=0; i < N1; ++i) {
        for (int j=0; j < N2; ++j) {
            for (int k=0; k < N3; ++k) {
                index.i = i;
                index.j = j;
                index.k = k;
                Location loc = grid->GetLocation(index);
                ASSERT_EQ(fy(loc.x, loc.y, loc.z), u2(index));
            }
        }
    }
}

void testClosures() {
    DataSourcePTR dataptr = DataSourceFactory::GetDataSource(1, 1, 1);
    DataSource* source = dataptr.get();
    int N1 = dataptr->Nx;
    int N2 = dataptr->Ny;
    int N3 = dataptr->Nz;
    GridShared grid = GridShared(new Grid(GridNormal, *source, 'p'));
    ArrayedFuncShared u = ArrayedFuncShared(new ArrayedFunc(grid, *source));
    u->FillFrom(&fx);
    GridFunctionShared sum = u + u;
    GridIndex index;
    for (int i=0; i < N1; ++i) {
        for (int j=0; j < N2; ++j) {
            for (int k=0; k < N3; ++k) {
                index.i = i;
                index.j = j;
                index.k = k;
                Location loc = grid->GetLocation(index);
                double v = fx(loc.x, loc.y, loc.z);
                ASSERT_EQ(v, u->operator()(index));
                ASSERT_EQ(2.*v, sum->operator()(index));
            }
        }
    }
    GridFunctionShared prod = u * u;
    for (int i=0; i < N1; ++i) {
        for (int j=0; j < N2; ++j) {
            for (int k=0; k < N3; ++k) {
                index.i = i;
                index.j = j;
                index.k = k;
                Location loc = grid->GetLocation(index);
                double v = fx(loc.x, loc.y, loc.z);
                ASSERT_EQ(v, u->operator()(index));
                ASSERT_EQ(v*v, prod->operator()(index));
            }
        }
    }
    const double val = 3.;
    GridFunctionShared cu = u * CF(val) + u;
    GridFunctionShared cu2 = u + u * CF(val);
    GridFunctionShared cp = (u + u) * CF(val);
    for (int i=0; i < N1; ++i) {
        for (int j=0; j < N2; ++j) {
            for (int k=0; k < N3; ++k) {
                index.i = i;
                index.j = j;
                index.k = k;
                Location loc = grid->GetLocation(index);
                double v = fx(loc.x, loc.y, loc.z);
                ASSERT_EQ(v, u->operator()(index));
                ASSERT_EQ(val*v + v, cu->operator()(index));
                ASSERT_EQ(cu2->operator()(index), cu->operator()(index));
                ASSERT_EQ((v+v)*val, cp->operator()(index));
            }
        }
    }
}

double fder(double x, double y, double z) {
    return x*y;
}

void testDerivative() {
    DataSourcePTR dataptr = DataSourceFactory::GetDataSource(1, 1, 1);
    DataSource* source = dataptr.get();
    int N1 = dataptr->Nx;
    int N2 = dataptr->Ny;
    int N3 = dataptr->Nz;
    GridShared grid = GridShared(new Grid(GridNormal, *source, 'p'));
    ArrayedFuncShared u = ArrayedFuncShared( new ArrayedFunc(grid, *source));
    u->FillFrom(&fx);
    GridIndex index;
    Derivative derX = Derivative(u, 1);
    Derivative derY = Derivative(u, 2);
    Derivative derZ = Derivative(u, 3);
    for (int i=1; i < N1-1; ++i) {
        for (int j=1; j < N2-1; ++j) {
            for (int k=1; k < N3-1; ++k) {
                index.i = i;
                index.j = j;
                index.k = k;
                Location loc = grid->GetLocation(index);
                double v = fx(loc.x, loc.y, loc.z);
                ASSERT_DOBL_EQ(v, u->operator()(index));
                ASSERT_DOBL_EQ(1, derX(index));
                ASSERT_DOBL_EQ(0, derY(index));
                ASSERT_DOBL_EQ(0, derZ(index));
            }
        }
    }
    u->FillFrom(&fder);
    for (int i=1; i < N1-1; ++i) {
        for (int j=1; j < N2-1; ++j) {
            for (int k=1; k < N3-1; ++k) {
                index.i = i;
                index.j = j;
                index.k = k;
                Location loc = grid->GetLocation(index);
                double v = fder(loc.x, loc.y, loc.z);
                ASSERT_DOBL_EQ(v, u->operator()(index));
                ASSERT_DOBL_EQ(loc.y, derX(index));
                ASSERT_DOBL_EQ(loc.x, derY(index));
                ASSERT_DOBL_EQ(0, derZ(index));
            }
        }
    }
}

void testCharacteristicFunction() {
    int i = 1;
    bool shift = false; //1f
    CharacteristicFunction charF(1, i, shift);
    GridIndex indx;
    indx.i = 0;
    indx.shiftX = true; //0t
    ASSERT_EQ(0, charF(indx));
    indx.shiftX = false; //0f
    ASSERT_EQ(0, charF(indx));
    indx.i = 1;
    indx.shiftX = true; //1t
    ASSERT_EQ(0.5, charF(indx));
    indx.shiftX = false; //1f
    ASSERT_EQ(1, charF(indx));
    indx.i = 2;
    indx.shiftX = true; //2t
    ASSERT_EQ(0.5, charF(indx));
    indx.shiftX = false; //2f
    ASSERT_EQ(0, charF(indx));
    indx.i = 3;
    indx.shiftX = true; //3t
    ASSERT_EQ(0, charF(indx));
}

int main() {
    testTMS();
    testSolver();
    testCyclicTMS();
    stressTest();
    test3dArray();
    testGrids();
    testGridFunctions();
    testGridFilling();
    testClosures();
    testDerivative();
    testCharacteristicFunction();
    return 0;
}
