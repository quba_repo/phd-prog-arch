#ifndef iterationsType_h
#define iterationsType_h

enum iterationType {
    iterationTypeDefault = 0,
    iterationTypeAllOnlyGlobals,
    iterationTypeAllOnlyPuasson,
    iterationTypeAll
};

#endif /* iterationsType_h */
