//
//  grid_type.h
//  partest
//
//  Created by Антон Кудряшов on 14/12/16.
//  Copyright © 2016 Антон Кудряшов. All rights reserved.
//

#ifndef grid_type_h
#define grid_type_h

enum gridType {
    GridNormal = 0,
    GridShiftedX = 1,
    GridShiftedY = 2,
    GridShiftedZ = 3,
    GridUnknown,
};

#endif /* grid_type_h */
