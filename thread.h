#ifndef thread_h
#define thread_h

#pragma once
#include <mpi.h>
#include <sstream>

#include "data_source.h"
#include "index.h"

//
// MPI Control panel
//
namespace Control {
    // Timer
    //
    class Timer {
     public:
        void Start() {
            startTime_ = MPI_Wtime();
        }
        double WastedTime() const {
            return MPI_Wtime() - startTime_;
        }
     private:
        double startTime_;
    };
    // Main thread object
    //
    class Thread {
     public:
        Thread(DataSourcePTR dataSource) :
        dataSource_(std::move(dataSource)) {
            const int p1 = dataSource_->Px;
            const int p2 = dataSource_->Py;
            const int p3 = dataSource_->Pz;
            MPI_Init(NULL, NULL);
            timer_ = Timer();
            timer_.Start();
            MPI_Comm_size(MPI_COMM_WORLD, &size_);
            MPI_Comm_rank(MPI_COMM_WORLD, &rank_);
            if (size_ != p1*p2*p3) {
                std::cerr << "representation error for size " << p1*p2*p3;
                std::cerr << " of " << size_ << std::endl;
                MPI_Abort(MPI_COMM_WORLD, 0);
            }
            int diff = rank_ % (p1 * p2);
            k_ = (rank_ - diff) / (p1 * p2);
            i_ = diff % p1;
            j_ = (diff - i_) / p1;
            if (rank_ != ((p1 * p2)*k_ + p1*j_ + i_)) {
                std::cerr << "representation error for rank " << rank_ << std::endl;
                MPI_Abort(MPI_COMM_WORLD, 0);
            }
            // commutators initialization
            colorX_ = p2*k_ + j_;
            MPI_Comm_split(MPI_COMM_WORLD, colorX_, rank_, &commX_);
            colorY_ = p2*p3 + p1*k_ + i_;
            MPI_Comm_split(MPI_COMM_WORLD, colorY_, rank_, &commY_);
            colorZ_ = p2*p3 + p1*p3 + p1*j_ + i_;
            MPI_Comm_split(MPI_COMM_WORLD, colorZ_, rank_, &commZ_);
            
            dataSource_->RankOffsetUpdate(GetRankOffset());
            
            MPI_Barrier(MPI_COMM_WORLD);
        }
        MPI_Comm Commutation(int direction) const {
            switch (direction) {
                case 1:
                    return commX_;
                case 2:
                    return commY_;
                case 3:
                    return commZ_;
                default:
                    break;
            }
            return MPI_COMM_NULL;
        }
        int RankWorld() const {
            return rank_;
        }
        int RankX() const {
            return i_;
        }
        int RankY() const {
            return j_;
        }
        int RankZ() const {
            return k_;
        }
        Index GetRankOffset() const {
            Index res(RankX(), RankY(), RankZ());
            return res;
        }
        int Rank(int direction) const {
            switch (direction) {
                case 1:
                    return RankX();
                case 2:
                    return RankY();
                case 3:
                    return RankZ();
                default:
                    break;
            }
            std::cerr << "rank is not found" << std::endl;
            MPI_Abort(MPI_COMM_WORLD, 0);
            return -1111;
        }
        int PWorld() const {
            return size_;
        }
        int Px() const {
            return dataSource_->Px;
        }
        int Py() const {
            return dataSource_->Py;
        }
        int Pz() const {
            return dataSource_->Pz;
        }
        int P(int direction) const {
            switch (direction) {
                case 1:
                    return Px();
                case 2:
                    return Py();
                case 3:
                    return Pz();
                default:
                    break;
            }
            std::cerr << "processors count is not found" << std::endl;
            MPI_Abort(MPI_COMM_WORLD, 0);
            return -1;
        }
        Index GetPIndex() const {
            Index res(Px(), Py(), Pz());
            return res;
        }
        void Stop() const {
            MPI_Finalize();
        }
        double WastedTime() const {
            return timer_.WastedTime();
        }
        const DataSource GetDataSource() const {
            return *dataSource_;
        }
     private:
        int size_;
        int rank_;
        // rank = p1*p2*k + p1*j + i
        // where are
        // i = 0,...,p1-1
        // j = 0,...,p2-1
        // k = 0,...,p3-1
        int i_;
        int j_;
        int k_;
        // commutators
        // x,y,z
        MPI_Comm commX_;
        MPI_Comm commY_;
        MPI_Comm commZ_;
        // associated colors with them
        int colorX_;
        int colorY_;
        int colorZ_;
        // main timer
        Timer timer_;
        // data source
        DataSourcePTR dataSource_;
    };
}

#endif /* thread_h */
