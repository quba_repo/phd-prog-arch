#include "data_source.h"

#include <iostream>

//
// filling functions here
// note: you can use all constants from data source here, cause
// it's instance method.
// But at scheme you need to use 1 instance of data source
//
double DataSource::p_0(double x, double y, double z) const {
    return 1 - 2.*x;
}
double DataSource::u_0(double x, double y, double z) const {
    return 1 - y*y;
}
double DataSource::v_0(double x, double y, double z) const {
    return 0;
}
double DataSource::w_0(double x, double y, double z) const {
    return 0;
}
//
// filling constants here
//
DataSourcePTR DataSourceFactory::GetDataSource(int Px, int Py, int Pz) {
    DataSourcePTR dataSource = DataSourcePTR(new DataSource());
    
    // processors
    dataSource->Px = Px;
    dataSource->Py = Py;
    dataSource->Pz = Pz;
    
    // cells
    dataSource->Nx = dataSource->Px*9 + 1;
    dataSource->Ny = 10;
    dataSource->Nz = 1;
    
    if ((dataSource->Nx-1)%dataSource->Px != 0) {
        std::cerr << "number of cells Nx-1= " << dataSource->Nx;
        std::cerr << " is not devided by Px= " << dataSource->Px << std::endl;
        std::terminate();
    }
    if ((dataSource->Ny-1)%dataSource->Py != 0) {
        std::cerr << "number of cells Ny-1= " << dataSource->Ny;
        std::cerr << " is not devided by Py= " << dataSource->Py << std::endl;
        std::terminate();
    }
    if ((dataSource->Nz-1)%dataSource->Pz != 0) {
        std::cerr << "number of cells Nz-1= " << dataSource->Nz;
        std::cerr << " is not devided by Pz= " << dataSource->Pz << std::endl;
        std::terminate();
    }
    dataSource->nlocx = (dataSource->Nx - 1)/dataSource->Px + 1;
    dataSource->nlocy = (dataSource->Ny - 1)/dataSource->Py + 1;
    dataSource->nlocz = (dataSource->Nz - 1)/dataSource->Pz + 1;;
    
    // cube
    dataSource->Ix = {0, 1};
    dataSource->Iy = {-1, 1};
    dataSource->Iz = {0, 1};
    
    dataSource->hx = dataSource->Ix.Width()/(dataSource->Nx - 1);
    dataSource->hy = dataSource->Iy.Width()/(dataSource->Ny - 1);
    dataSource->hz = dataSource->Iz.Width()/(dataSource->Nz - 1);
    
    // scheme parameters
    // alpha
    dataSource->alpha = 0.6;
    dataSource->alphaPuasson = dataSource->alpha;
    // tau
    dataSource->tau = 0.1;
    dataSource->tauPuasson = dataSource->tau;
    // max time steps
    dataSource->maxSteps = 100;
    dataSource->maxStepsPuasson = 10;
    // precision
    dataSource->epsilon = 0.001;
    dataSource->epsilonPuasson = 0.001;
    // iterations type
    dataSource->itersType = iterationTypeDefault;
    
    // bounds
    // P-X
    dataSource->boundsP.Lx = { BoundDirichlet, 1 };
    dataSource->boundsP.Rx = { BoundDirichlet, -1 };
    // P-Y
    dataSource->boundsP.Ly = { BoundDirichlet, dataSource->nlocx, dataSource->nlocz };
    dataSource->boundsP.Ry = { BoundDirichlet, dataSource->nlocx, dataSource->nlocz };
    // P-Z
    dataSource->boundsP.Lz = { BoundDirichlet, dataSource->nlocx, dataSource->nlocy };
    dataSource->boundsP.Rz = { BoundDirichlet, dataSource->nlocx, dataSource->nlocy };
    
    // U-X
    dataSource->boundsU.Lx = { BoundDirichlet, dataSource->nlocy, dataSource->nlocz };
    dataSource->boundsU.Rx = { BoundDirichlet, dataSource->nlocy, dataSource->nlocz };
    // U-Y
    dataSource->boundsU.Ly = { BoundDirichlet, 0 };
    dataSource->boundsU.Ry = { BoundDirichlet, 0 };
    // U-Z
    dataSource->boundsU.Lz = { BoundDirichlet, 0 };
    dataSource->boundsU.Rz = { BoundDirichlet, 0 };
    // V-X
    dataSource->boundsV.Lx = { BoundDirichlet, 0 };
    dataSource->boundsV.Rx = { BoundDirichlet, 0 };
    // V-Y
    dataSource->boundsV.Ly = { BoundDirichlet, 0 };
    dataSource->boundsV.Ry = { BoundDirichlet, 0 };
    // V-Z
    dataSource->boundsV.Lz = { BoundDirichlet, 0 };
    dataSource->boundsV.Rz = { BoundDirichlet, 0 };
    // W-X
    dataSource->boundsW.Lx = { BoundDirichlet, 0 };
    dataSource->boundsW.Rx = { BoundDirichlet, 0 };
    // W-Y
    dataSource->boundsW.Ly = { BoundDirichlet, 0 };
    dataSource->boundsW.Ry = { BoundDirichlet, 0 };
    // W-Z
    dataSource->boundsW.Lz = { BoundDirichlet, 0 };
    dataSource->boundsW.Rz = { BoundDirichlet, 0 };
    
    return dataSource;
}
void DataSource::RankOffsetUpdate(Index rankOffset) {
    Index offset = Index(rankOffset.i*(nlocx - 1), rankOffset.j*(nlocy - 1), rankOffset.k*(nlocz - 1));
    std::cout << "bounds are updated with " << rankOffset.Description() << std::endl;
    Bound* b;
    Location locL, locR;
    locL.x = Ix.a + offset.i*hx;
    locR.x = Ix.a + (offset.i + nlocx - 1)*hx;
    for (int j = 0; j < nlocy; ++j) {
        locL.y = Iy.a + (offset.j + j)*hy;
        locR.y = Iy.a + (offset.j + j)*hy;
        for (int k = 0; k < nlocz; ++k) {
            locL.z = Iz.a + (offset.k + k)*hz;
            locR.z = Iz.a + (offset.k + k)*hz;
            // x-bounds
            
            b = &boundsU.Lx;
            b->layer->SetValue(j, k, u_0(locL.x, locL.y, locL.z));
            b = &boundsU.Rx;
            b->layer->SetValue(j, k, u_0(locR.x, locR.y, locR.z));
            
            // b->layer.SetValue(j, k, p_0(locL.x, locL.y, locL.z));
            // b->layer.SetValue(j, k, (p_0(locL.x + hx, locL.y, locL.z) - p_0(locL.x, locL.y, locL.z))/hx);
        }
    }
    
    locL.y = Iy.a + offset.j*hy;
    locR.y = Iy.a + (offset.j + nlocy - 1)*hy;
    for (int i = 0; i < nlocx; ++i) {
        locL.x = Ix.a + (offset.i + i)*hx;
        locR.x = Ix.a + (offset.i + i)*hx;
        for (int k = 0; k < nlocz; ++k) {
            locL.z = Iz.a + (offset.k + k)*hz;
            locR.z = Iz.a + (offset.k + k)*hz;
            // y-bounds
            b = &boundsP.Ly;
            b->layer->SetValue(i, k, p_0(locL.x, locL.y, locL.z));
            b = &boundsP.Ry;
            b->layer->SetValue(i, k, p_0(locR.x, locR.y, locR.z));
        }
    }
    
    locL.z = Iz.a + (offset.k)*hz;
    locR.z = Iz.a + (offset.k + nlocz - 1)*hz;
    for (int i = 0; i < nlocx; ++i) {
        locL.x = Ix.a + (offset.i + i)*hx;
        locR.x = Ix.a + (offset.i + i)*hx;
        for (int j = 0; j < nlocy; ++j) {
            locL.y = Iy.a + (offset.j + j)*hy;
            locR.y = Iy.a + (offset.j + j)*hy;
            // z-bounds
            b = &boundsP.Lz;
            b->layer->SetValue(i, j, p_0(locL.x, locL.y, locL.z));
            b = &boundsP.Rz;
            b->layer->SetValue(i, j, p_0(locR.x, locR.y, locR.z));
        }
    }
}

void BoundTupleDescription(const BoundTuple& tuple, const std::string& name) {
    std::cout << name << ":\tLx[" << tuple.Lx.Description() << "], ";
    std::cout << "\tRx[" << tuple.Rx.Description() << "], " << std::endl;
    std::cout << "\t\tLy[" << tuple.Ly.Description() << "], ";
    std::cout << "\tRy[" << tuple.Ry.Description() << "], " << std::endl;
    std::cout << "\t\tLz[" << tuple.Lz.Description() << "], ";
    std::cout << "\tRz[" << tuple.Rz.Description() << "], " << std::endl;
}

void DataSource::PrintDescription() const {
    std::cout << "!Description" << std::endl;
    std::cout << "\t";
    std::cout << "Processors: " << Px << ", " << Py << ", " << Pz << std::endl;
    std::cout << "\t";
    std::cout << "Number of Cells: " << Nx << ", " << Ny << ", " << Nz << std::endl;
    std::cout << "\t";
    std::cout << "Domain: [" << Ix.a << ", " << Ix.b << "], ";
    std::cout << "[" << Iy.a << ", " << Iy.b << "], ";
    std::cout << "[" << Iz.a << ", " << Iz.b << "]" << std::endl;
    std::cout << "\t";
    std::cout << "Bounds conditions" << std::endl;
    std::cout << "\t";
    BoundTupleDescription(boundsP, "P");
    std::cout << "\t";
    BoundTupleDescription(boundsU, "U");
    std::cout << "\t";
    BoundTupleDescription(boundsV, "V");
    std::cout << "\t";
    BoundTupleDescription(boundsW, "W");
    std::cout << "Scheme parameters" << std::endl;
    std::cout << "\t";
    std::cout << "Global:steps="<< maxSteps << "alpha=" << alpha
    << ", tau=" << tau << std::endl;
    std::cout << "\t";
    std::cout << "Puasson:steps="<< maxStepsPuasson << "alpha=" << alphaPuasson
    << ", tau=" << tauPuasson << std::endl;
    std::cout << "!End description" << std::endl;
}
