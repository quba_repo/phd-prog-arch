//
//  bound.h
//  partest
//
//  Created by Антон Кудряшов on 14/12/16.
//  Copyright © 2016 Антон Кудряшов. All rights reserved.
//

#ifndef bound_h
#define bound_h

#include "layer.h"
#include <sstream>

enum boundtype { BoundDirichlet, BoundNeumann, BoundCyclic };

struct Bound {
    boundtype type;
    std::shared_ptr<BoundLayer> layer;
    Bound(boundtype type_ = BoundDirichlet, double value = 0.) {
        type = type_;
        layer = std::shared_ptr<BoundLayer>(new BoundLayer(1, 1));
        layer->SetValue(0, 0, value);
    }
    Bound(boundtype type_, int N2, int N3) {
        type = type_;
        layer =std::shared_ptr<BoundLayer>( new BoundLayer(N2, N3));
        N2_ = N2;
        N3_ = N3;
    }
    
    std::string Description() const {
        std::stringstream ss;
        switch(type) {
            case BoundDirichlet:
                ss << "Dirichlet";
                break;
            case BoundNeumann:
                ss << "Neumann";
                break;
            case BoundCyclic:
                ss << "Cyclic";
                break;
            default:
                ss << "Unknown";
                break;
        }
        ss << ", with size: " << layer->size();
        return ss.str();
    }
    double Value(int j, int k) const {
        if (layer->size() == 1) {
            return layer->Value(0, 0);
        }
        return layer->Value(j, k);
    }
 private:
    int N2_, N3_;
};

struct BoundTuple {
    Bound Lx;
    Bound Rx;
    Bound Ly;
    Bound Ry;
    Bound Lz;
    Bound Rz;
};

#endif /* bound_h */
