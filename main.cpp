#ifndef PARALLEL_MODE
#define PARALLEL_MODE
#endif
#include "macroses.h"

#include <iostream>

#include "data_source.h"
#include "par_slae_tests.h"
#include "factorization.h"
#include "scheme_tests.h"

int main(int argc, char* argv[]) {
    
    if (argc != 4) {
        std::cerr << "must be initialized 3 proc. parameters Px*Py*Pz = N";
        std::cerr << std::endl;
        return -1;
    }
    int Px, Py, Pz;
    sscanf(argv[1], "%d", &Px);
    sscanf(argv[2], "%d", &Py);
    sscanf(argv[3], "%d", &Pz);
    
    DataSourcePTR dataSource = DataSourceFactory::GetDataSource(Px, Py, Pz);
    
    std::shared_ptr<Control::Thread> threadPtr(new Control::Thread(std::move(dataSource)));
    { // start
        LaunchSLAETests(*threadPtr);
        FactorizationScheme scheme(threadPtr);
        TestScheme(scheme, threadPtr);
        scheme.Solve();
    } // end
    threadPtr->Stop();
    return 0;
}
