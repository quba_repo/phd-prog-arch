//
//  location.h
//  partest
//
//  Created by Антон Кудряшов on 16/01/17.
//  Copyright © 2017 Антон Кудряшов. All rights reserved.
//

#ifndef location_h
#define location_h

struct Location {
    double x;
    double y;
    double z;
};

#endif /* location_h */
