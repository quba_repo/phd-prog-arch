#include "mpi_utils.h"

#include <string>
#include <sstream>
#include <fstream>

void Utils::FilePrint(MPI_Comm commutation, const std::stringstream& stream,
                      const std::string& filename,
                      bool barrier) {
    int size, rank;
    MPI_Comm_size(commutation, &size);
    MPI_Comm_rank(commutation, &rank);
    bool print = rank == 0 || size == 1;
    int tag = 6;
    std::ios_base::openmode mode = rank == 0 ? std::ios::out : std::ios::app;
    if (!print) {
        MPI_Status stat;
        MPI_Recv(&print, 1, MPI_C_BOOL, rank - 1, tag, commutation, &stat);
    }
    std::fstream file;
    file.open(filename, mode);
    file << stream.str();
    file << std::endl;
    file.close();
    if (rank < size - 1) {
        MPI_Send(&print, 1, MPI_C_BOOL, rank + 1, tag, commutation);
    }
    
    if (barrier) {
        MPI_Barrier(commutation);
    }
}

void Utils::SerialPrint(MPI_Comm commutation, const std::stringstream& stream,
                        bool barrier) {
    int size, rank;
    MPI_Comm_size(commutation, &size);
    MPI_Comm_rank(commutation, &rank);
    bool print = rank == 0;
    int tag = 1;
    if (!print) {
        MPI_Status stat;
        MPI_Recv(&print, 1, MPI_C_BOOL, rank - 1, tag, commutation, &stat);
    }
    std::cout << stream.str();
    if (rank < size - 1) {
        MPI_Send(&print, 1, MPI_C_BOOL, rank + 1, tag, commutation);
    }
    if (barrier) {
        MPI_Barrier(commutation);
    }
}
//
void Utils::ArrayPrint(MPI_Comm commutation, const std::vector<double>& v,
                       const std::string& name, bool barrier) {
    std::stringstream ss;
    int rank;
    MPI_Comm_rank(commutation, &rank);
    for (int i=0; i < v.size(); ++i) {
        ss << name << "[" << i + rank*v.size() - rank << "] =\t";
        ss << v[i] << std::endl;
    }
    Utils::SerialPrint(commutation, ss, barrier);
}
void Utils::ArrayPrint(MPI_Comm commutation, double* v, int N,
                       const std::string& name, bool barrier) {
    std::stringstream ss;
    int rank;
    MPI_Comm_rank(commutation, &rank);
    for (int i=0; i < N; ++i) {
        ss << name << "[" << i + rank*N - rank << "] =\t";
        ss << v[i] << std::endl;
    }
    Utils::SerialPrint(commutation, ss, barrier);
}
