//
//  factorization.cpp
//  partest
//
//  Created by Антон Кудряшов on 26/01/17.
//  Copyright © 2017 Антон Кудряшов. All rights reserved.
//

#include "factorization.h"

#include "mpi_utils.h"
#include "slae-parallel.h"
#include "file_handler.h"

#include <math.h>

double Functions::FindMaxError(Reaper reaper, char* symbol) {
    double max = -1;
    FOR_IN_all_MACRO(reaper, {
        double value = fabs(p->operator()(reaper.normal));
        if(value > max) {
            max = value;
            *symbol = 'p';
        }
        value = fabs(u->operator()(reaper.xshift));
        if(value > max) {
            max = value;
            *symbol = 'u';
        }
        value = fabs(v->operator()(reaper.yshift));
        if(value > max) {
            max = value;
            *symbol = 'v';
        }
        value = fabs(w->operator()(reaper.zshift));
        if(value > max) {
            max = value;
            *symbol = 'w';
        }
    });
    double max_res;
    MPI_Allreduce(&max, &max_res, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
    return max_res;
}

FactorizationScheme::FactorizationScheme(std::shared_ptr<Control::Thread> threadPtr) :
threadPtr_(threadPtr) {
    
    const DataSource ds = threadPtr_->GetDataSource();
    if (threadPtr->RankWorld() == 0) {
        ds.PrintDescription();
    }
    Control::Timer timer;
    timer.Start();
    Index rankOffset = threadPtr_->GetRankOffset();
    
    mu_ = CF(1);
    
    f_.p = ArrayedFuncShared(new ArrayedFunc(GridShared(new Grid(GridNormal  , ds, 'p', rankOffset)), ds, 'p'));
    f_.u = ArrayedFuncShared(new ArrayedFunc(GridShared(new Grid(GridShiftedX, ds, 'u', rankOffset)), ds, 'u'));
    f_.v = ArrayedFuncShared(new ArrayedFunc(GridShared(new Grid(GridShiftedY, ds, 'v', rankOffset)), ds, 'v'));
    f_.w = ArrayedFuncShared(new ArrayedFunc(GridShared(new Grid(GridShiftedZ, ds, 'w', rankOffset)), ds, 'w'));
    
    f_.p->FillFrom(&DataSource::p_0, ds);
    f_.u->FillFrom(&DataSource::u_0, ds);
    f_.v->FillFrom(&DataSource::v_0, ds);
    f_.w->FillFrom(&DataSource::w_0, ds);
    
    {
        FunctionPrinter file("out/p_0.txt");
        file.PrintFunction(f_.p, threadPtr_);
        FunctionPrinter file2("out/u_0.txt");
        file2.PrintFunction(f_.u, threadPtr_);
        FunctionPrinter file3("out/v_0.txt");
        file3.PrintFunction(f_.v, threadPtr_);
        FunctionPrinter file4("out/w_0.txt");
        file4.PrintFunction(f_.w, threadPtr_);
    }
    
    f_.p->UpdateBounds(*threadPtr_);
    f_.u->UpdateBounds(*threadPtr_);
    f_.v->UpdateBounds(*threadPtr_);
    f_.w->UpdateBounds(*threadPtr_);
    
    {
        FunctionPrinter file("out/p_0u.txt");
        file.PrintFunction(f_.p, threadPtr_);
        FunctionPrinter file2("out/u_0u.txt");
        file2.PrintFunction(f_.u, threadPtr_);
        FunctionPrinter file3("out/v_0u.txt");
        file3.PrintFunction(f_.v, threadPtr_);
        FunctionPrinter file4("out/w_0u.txt");
        file4.PrintFunction(f_.w, threadPtr_);
    }
    
    std::cout << "Functions initialized at " << threadPtr_->RankWorld() << " with ";
    std::cout << timer.WastedTime() << "sec" << std::endl;
}

bool FactorizationScheme::IterationCondition_(bool globals, int iterations,
                                              double distance) {
    
    distance = fabs(distance);
    const DataSource ds = threadPtr_->GetDataSource();
    int const option = ds.itersType;
    double eps = globals ? ds.epsilon : ds.epsilonPuasson;
    switch (option) {
        case iterationTypeAllOnlyGlobals:
            return globals ? iterations > 0 : iterations > 0 && distance > eps;
        case iterationTypeAllOnlyPuasson:
            return !globals ? iterations > 0 : iterations > 0 && distance > eps;
        case iterationTypeAll:
            return iterations > 0;
        case iterationTypeDefault:
        default:
            return iterations > 0 && distance > eps;
    }
}

void FactorizationScheme::Solve() {
    
    const DataSource ds = threadPtr_->GetDataSource();
    Functions ksi;
    ksi.p = ArrayedFuncShared(new ArrayedFunc(f_.p->GetGridShared(), ds, '0'));
    ksi.u = ArrayedFuncShared(new ArrayedFunc(f_.u->GetGridShared(), ds, '1'));
    ksi.v = ArrayedFuncShared(new ArrayedFunc(f_.v->GetGridShared(), ds, '2'));
    ksi.w = ArrayedFuncShared(new ArrayedFunc(f_.w->GetGridShared(), ds, '3'));
    
    {
        FunctionPrinter fileks("out/ksip_0.txt");
        fileks.PrintFunction(ksi.p, threadPtr_);
    }
    ksi.p->UpdateBounds(*threadPtr_);
    {
        FunctionPrinter fileks("out/ksip_0u.txt");
        fileks.PrintFunction(ksi.p, threadPtr_);
    }
    
    double distance = -10;
    int iterations = ds.maxSteps;
    int k = 1;
    while (IterationCondition_(true, iterations, distance)) {
        Step0_(ksi);
        StepPuasson_(ksi);
        StepX_(ksi);
        StepY_(ksi);
        StepZ_(ksi);
        StepFinal_(ksi);
        iterations--;
        
        char symbol;
        double value = ksi.FindMaxError(GetReaper(), &symbol);
        if (threadPtr_->RankWorld() == 0) {
            printf("max error is %.15lf at ksi.%c\n", value, symbol);
        }
        k++;
    }
    {
        FunctionPrinter fileks("out/ksip_fin.txt");
        fileks.PrintFunction(ksi.p, threadPtr_);

        FunctionPrinter file("out/p_fin.txt");
        file.PrintFunction(f_.p, threadPtr_);
        FunctionPrinter file2("out/u_fin.txt");
        file2.PrintFunction(f_.u, threadPtr_);
        FunctionPrinter file3("out/v_fin.txt");
        file3.PrintFunction(f_.v, threadPtr_);
        FunctionPrinter file4("out/w_fin.txt");
        file4.PrintFunction(f_.w, threadPtr_);
    }
}

void FactorizationScheme::Step0_(Functions ksi) {
    
    Control::Timer timer;
    timer.Start();
    const double pdiv = 0.5;
    
    auto ksi_p_out = DerF(f_.u, 'X') + DerF(f_.v, 'Y') + DerF(f_.w, 'Z');
    
    auto ksi_u_out =
      DerF(CF(1.-pdiv)*f_.u*f_.u + f_.p - CF(2)*mu_*DerF(f_.u, 'X'), 'X')         + CF(pdiv)*f_.u*DerF(f_.u, 'X')
    + DerF(CF(1.-pdiv)*f_.u*f_.v - mu_*(DerF(f_.u, 'Y') + DerF(f_.v, 'X') ), 'Y') + CF(pdiv)*f_.v*DerF(f_.u, 'Y')
    + DerF(CF(1.-pdiv)*f_.u*f_.w - mu_*(DerF(f_.u, 'Z') + DerF(f_.w, 'X') ), 'Z') + CF(pdiv)*f_.w*DerF(f_.u, 'Z');
    
    auto ksi_v_out =
      DerF(CF(1.-pdiv)*f_.v*f_.u - mu_*(DerF(f_.u, 'Y') + DerF(f_.v, 'X') ), 'X') + CF(pdiv)*f_.u*DerF(f_.v, 'X')
    + DerF(CF(1.-pdiv)*f_.v*f_.v + f_.p - CF(2)*mu_*DerF(f_.v, 'Y'), 'Y')         + CF(pdiv)*f_.v*DerF(f_.v, 'Y')
    + DerF(CF(1.-pdiv)*f_.v*f_.w - mu_*(DerF(f_.v, 'Z') + DerF(f_.w, 'Y') ), 'Z') + CF(pdiv)*f_.w*DerF(f_.v, 'Z');
    
    auto ksi_w_out =
      DerF(CF(1.-pdiv)*f_.w*f_.u - mu_*(DerF(f_.u, 'Z') + DerF(f_.w, 'X') ), 'X') + CF(pdiv)*f_.u*DerF(f_.w, 'X')
    + DerF(CF(1.-pdiv)*f_.w*f_.v - mu_*(DerF(f_.v, 'Z') + DerF(f_.w, 'Y') ), 'Y') + CF(pdiv)*f_.v*DerF(f_.w, 'Y')
    + DerF(CF(1.-pdiv)*f_.w*f_.w + f_.p - CF(2)*mu_*DerF(f_.w, 'Z'), 'Z')         + CF(pdiv)*f_.w*DerF(f_.w, 'Z');
    
    Reaper reaper = GetReaper();
    
    FOR_IN_interior_MACRO(reaper, {
        ksi.p->operator[](reaper.normal) = ksi_p_out->operator()(reaper.normal);
        ksi.u->operator[](reaper.xshift) = ksi_u_out->operator()(reaper.xshift);
        ksi.v->operator[](reaper.yshift) = ksi_v_out->operator()(reaper.yshift);
        ksi.w->operator[](reaper.zshift) = ksi_w_out->operator()(reaper.zshift);
    });
    
    ksi.p->UpdateBounds(*threadPtr_);
    ksi.u->UpdateBounds(*threadPtr_);
    ksi.v->UpdateBounds(*threadPtr_);
    ksi.w->UpdateBounds(*threadPtr_);
    
//    std::cout << "Step0 finished at " << threadPtr_->RankWorld() << " with ";
//    std::cout << timer.WastedTime() << "sec" << std::endl;
}

void FactorizationScheme::StepPuasson_(Functions ksi) {
    
    Control::Timer timer;
    timer.Start();
    
    DataSource ds = threadPtr_->GetDataSource();
    double distance = -10;
    int iterations = ds.maxStepsPuasson;
    ArrayedFuncShared ksip0 = ArrayedFuncShared(new ArrayedFunc(f_.p->GetGridShared(), ds, '0'));
    int current = 0;
    while (IterationCondition_(false, iterations, distance)) {
        current++;
        double distance2 = SolvePuasson_(ksi, ksip0);
        double max;
        MPI_Allreduce(&distance2, &max, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
        if (max < ds.epsilonPuasson) {
            iterations = 1;
        } else if (distance > 0) {
            double dd = fabs(distance - max)/ds.tauPuasson;
            max = std::max(dd, max);
            if (max < ds.epsilonPuasson) {
                iterations = 1;
            }
        }
        distance = max;
        iterations--;
        if (threadPtr_->RankWorld() == 0) {
            printf("#puasson\t%d\t\t%.15f\n", current, distance);
        }
        MPI_Barrier(MPI_COMM_WORLD);
    }
    
    const double ta_gl = ds.tau*ds.alpha;
    auto ksi_u_out = CF(ta_gl)*DerF(ksip0, 'X');
    auto ksi_v_out = CF(ta_gl)*DerF(ksip0, 'Y');
    auto ksi_w_out = CF(ta_gl)*DerF(ksip0, 'Z');
    
    Reaper reaper = GetReaper();
    FOR_IN_interior_MACRO(reaper, {
        ksi.p->operator[](reaper.normal)  = ksip0->operator()(reaper.normal);
        ksi.u->operator[](reaper.xshift) -= ksi_u_out->operator()(reaper.xshift);
        ksi.v->operator[](reaper.yshift) -= ksi_v_out->operator()(reaper.yshift);
        ksi.w->operator[](reaper.zshift) -= ksi_w_out->operator()(reaper.zshift);
    });
    
    ksi.p->UpdateBounds(*threadPtr_);
    ksi.u->UpdateBounds(*threadPtr_);
    ksi.v->UpdateBounds(*threadPtr_);
    ksi.w->UpdateBounds(*threadPtr_);
    
//    std::cout << "StepPuasson_ finished at " << threadPtr_->RankWorld() << " with ";
//    std::cout << timer.WastedTime() << "sec" << std::endl;
}

double FactorizationScheme::SolvePuasson_(const Functions& ksi,
                                          ArrayedFuncShared ksi_p_0) {
    DataSource ds = threadPtr_->GetDataSource();
    GridShared gridPtr = ksi_p_0->GetGridShared();
    ArrayedFuncShared dzeta_ksi = ArrayedFuncShared(new ArrayedFunc(gridPtr, ds, '0'));
    {
        const double ta_gl = ds.tau*ds.alpha;
        const double rev_ta_gl = 1./ta_gl;
        auto phi = CF(rev_ta_gl)*(  DerF(ksi.u, 'X')
                                  + DerF(ksi.v, 'Y')
                                  + DerF(ksi.w, 'Z')
                                  ) - CF(rev_ta_gl*rev_ta_gl)*ksi.p;
        auto dzeta_ksi_out =
        DerF(DerF(ksi_p_0, 'X'), 'X')
        + DerF(DerF(ksi_p_0, 'Y'), 'Y')
        + DerF(DerF(ksi_p_0, 'Z'), 'Z')
        - phi;
        
        Reaper reaper = GetReaper();
        FOR_IN_interior_MACRO(reaper, {
            dzeta_ksi->operator[](reaper.normal) = dzeta_ksi_out->operator()(reaper.normal);
        });
    }
    dzeta_ksi->UpdateBounds(*threadPtr_);
    // dzeta_ksi.removeAverage();
    
    int direction = 1;
    double ta_pu = ds.tauPuasson*ds.alphaPuasson;
    {
        CharacteristicFShared charF = CharacteristicFShared(new CharacteristicFunction(direction, gridPtr));
        auto expr1 = charF - CF(ta_pu)*DerF(DerF(charF, 'X'), 'X');
        RevertMatrix(expr1, charF, dzeta_ksi, threadPtr_, direction);
    }
    dzeta_ksi->UpdateOuter(direction, *threadPtr_);
    // dzeta_ksi.removeAverage();
    
    direction = 2;
    {
        CharacteristicFShared charF = CharacteristicFShared(new CharacteristicFunction(direction, gridPtr));
        auto expr2 = charF - CF(ta_pu)*DerF(DerF(charF, 'Y'), 'Y');
        RevertMatrix(expr2, charF, dzeta_ksi, threadPtr_, direction);
    }
    dzeta_ksi->UpdateOuter(direction, *threadPtr_);
    // dzeta_ksi.removeAverage();
    
    direction = 3;
    {
        CharacteristicFShared charF = CharacteristicFShared(new CharacteristicFunction(direction, gridPtr));
        auto expr3 = charF - CF(ta_pu)*DerF(DerF(charF, 'Z'), 'Z');
        RevertMatrix(expr3, charF, dzeta_ksi, threadPtr_, direction);
    }
    dzeta_ksi->UpdateOuter(direction, *threadPtr_);
    // dzeta_ksi.removeAverage();
    
    double max=0;
    Reaper reaper = GetReaper();
    FOR_IN_all_MACRO(reaper, {
        ksi_p_0->operator[](reaper.normal) += ds.tauPuasson*dzeta_ksi->operator()(reaper.normal);
        double value = fabs(ksi_p_0->operator()(reaper.normal));
        if(value > max) {
            max = value;
        }
    });
    
    return max;
}

void FactorizationScheme::StepX_(Functions ksi) {
    
    Control::Timer timer;
    timer.Start();
    
    DataSource ds = threadPtr_->GetDataSource();
    const int direction = 1;
    const double ta_gl = ds.tau*ds.alpha;
    GridShared gridPtr = ksi.u->GetGridShared();
    
    ConstantFShared b = CF(2);
    CharacteristicFShared charF = CharacteristicFShared(new CharacteristicFunction(direction, gridPtr));
    auto expr = charF + CF(ta_gl)*(f_.u*DerF(charF, 'X') - DerF(b*mu_*DerF(charF, 'X'), 'X'));
    
    RevertMatrix(expr, charF, ksi.u, threadPtr_, direction);
    ksi.u->UpdateBounds(*threadPtr_);
    
    b->SetValue(1);
    RevertMatrix(expr, charF, ksi.v, threadPtr_, direction);
    ksi.v->UpdateBounds(*threadPtr_);
    
    RevertMatrix(expr, charF, ksi.w, threadPtr_, direction);
    ksi.w->UpdateBounds(*threadPtr_);
    
//    std::cout << "StepX_ finished at " << threadPtr_->RankWorld() << " with ";
//    std::cout << timer.WastedTime() << "sec" << std::endl;
    
    MPI_Barrier(MPI_COMM_WORLD);
}

void FactorizationScheme::StepY_(Functions ksi) {
    
    Control::Timer timer;
    timer.Start();
    
    DataSource ds = threadPtr_->GetDataSource();
    const int direction = 2;
    const double ta_gl = ds.tau*ds.alpha;
    GridShared gridPtr = ksi.v->GetGridShared();
    
    ConstantFShared b = CF(1);
    CharacteristicFShared charF = CharacteristicFShared(new CharacteristicFunction(direction, gridPtr));
    auto expr = charF + CF(ta_gl)*(f_.v*DerF(charF, 'Y') - DerF(b*mu_*DerF(charF, 'Y'), 'Y'));
    
    RevertMatrix(expr, charF, ksi.u, threadPtr_, direction);
    ksi.u->UpdateBounds(*threadPtr_);
    
    b->SetValue(2);
    RevertMatrix(expr, charF, ksi.v, threadPtr_, direction);
    ksi.v->UpdateBounds(*threadPtr_);
    
    b->SetValue(1);
    RevertMatrix(expr, charF, ksi.w, threadPtr_, direction);
    ksi.w->UpdateBounds(*threadPtr_);
    
//    std::cout << "StepY_ finished at " << threadPtr_->RankWorld() << " with ";
//    std::cout << timer.WastedTime() << "sec" << std::endl;
    
    MPI_Barrier(MPI_COMM_WORLD);
}

void FactorizationScheme::StepZ_(Functions ksi) {
    
    Control::Timer timer;
    timer.Start();
    
    DataSource ds = threadPtr_->GetDataSource();
    const int direction = 3;
    const double ta_gl = ds.tau*ds.alpha;
    GridShared gridPtr = ksi.w->GetGridShared();
    
    ConstantFShared b = CF(1);
    CharacteristicFShared charF = CharacteristicFShared(new CharacteristicFunction(direction, gridPtr));
    auto expr = charF + CF(ta_gl)*(f_.w*DerF(charF, 'Z') - DerF(b*mu_*DerF(charF, 'Z'), 'Z'));
    
    RevertMatrix(expr, charF, ksi.u, threadPtr_, direction);
    ksi.u->UpdateBounds(*threadPtr_);
    
    RevertMatrix(expr, charF, ksi.v, threadPtr_, direction);
    ksi.v->UpdateBounds(*threadPtr_);
    
    b->SetValue(2);
    RevertMatrix(expr, charF, ksi.w, threadPtr_, direction);
    ksi.w->UpdateBounds(*threadPtr_);
    
//    std::cout << "StepZ_ finished at " << threadPtr_->RankWorld() << " with ";
//    std::cout << timer.WastedTime() << "sec" << std::endl;
    
    MPI_Barrier(MPI_COMM_WORLD);
}

void FactorizationScheme::StepFinal_(const Functions &ksi) {
    Control::Timer timer;
    timer.Start();
    
    Reaper reaper = GetReaper();
    FOR_IN_all_MACRO(reaper, {
        f_.p->operator[](reaper.normal) += ksi.p->operator()(reaper.normal);
        f_.u->operator[](reaper.xshift) += ksi.v->operator()(reaper.xshift);
        f_.v->operator[](reaper.yshift) += ksi.u->operator()(reaper.yshift);
        f_.w->operator[](reaper.zshift) += ksi.w->operator()(reaper.zshift);
    });
    
    f_.p->UpdateBounds(*threadPtr_);
    f_.u->UpdateBounds(*threadPtr_);
    f_.v->UpdateBounds(*threadPtr_);
    f_.w->UpdateBounds(*threadPtr_);
    
//    std::cout << "StepFinal_ finished at " << threadPtr_->RankWorld() << " with ";
//    std::cout << timer.WastedTime() << "sec" << std::endl;
    
    MPI_Barrier(MPI_COMM_WORLD);
}

Reaper FactorizationScheme::GetReaper() const {
    Reaper reaper(f_.p->grid.N_loc(1),
                  f_.p->grid.N_loc(2),
                  f_.p->grid.N_loc(3),
                  threadPtr_->GetRankOffset(), threadPtr_->GetPIndex());
    return reaper;
}

void RevertMatrix(GridFunctionShared expr,
                  CharacteristicFShared chf,
                  ArrayedFuncShared fout,
                  std::shared_ptr<Control::Thread> thread,
                  int direction) {
    
    int direction2, direction3;
    FindOtherDirections(direction, &direction2, &direction3);
    const int N = fout->grid.N_loc(direction);
    
    if (N == 1) {
        // there is no reverting
        return;
    }
    
    std::vector<double> l, c, r, d, x, z;
    std::vector<double> l0, c0, r0, d0;
    GridIndex index;
    if (fout->GridType() > GridNormal) {
        index.SetShift(true, fout->GridType());
    }
    index.directionP = direction;
    const int N2 = fout->grid.N_loc(direction2);
    const int N3 = fout->grid.N_loc(direction3);
    for (int j = 0; j < N2; ++j) {
        index.SetI(j, direction2);
        for (int k = 0; k < N3; ++k) {
            index.SetI(k, direction3);
            Utils::FillDataForSlae(thread->Commutation(direction), N,
                                   &l, &c, &r, &d,
                                   &l0, &c0, &r0, &d0,
                                   [&](int i, int loc, int rank, int size) {
                                       int idx = i;
                                       if (rank == 0 && i != loc) {
                                           idx = loc;
                                           if (i > 1) {
                                               index.rankP = i - 1;
                                           }
                                       }
                                       else {
                                           index.rankP = -1;
                                       }
                                       index.SetI(idx, direction);
                                       chf->SetIndex(idx-1);
                                       return expr->operator()(index);
                                   },
                                   [&](int i, int loc, int rank, int size) {
                                       int idx = i;
                                       if (rank == 0 && i != loc) {
                                           idx = loc;
                                           if (i > 1) {
                                               index.rankP = i - 1;
                                           }
                                       }
                                       else {
                                           index.rankP = -1;
                                       }
                                       chf->SetIndex(idx);
                                       return expr->operator()(index);
                                   },
                                   [&](int i, int loc, int rank, int size) {
                                       int idx = i;
                                       if (rank == 0 && i != loc) {
                                           idx = loc;
                                           if (i > 1) {
                                               index.rankP = i - 1;
                                           }
                                       }
                                       else {
                                           index.rankP = -1;
                                       }
                                       chf->SetIndex(idx+1);
                                       return expr->operator()(index);
                                   },
                                   [&](int i, int loc, int rank, int size) {
                                       if (rank == 0 && loc > N) {
                                           BoundLayer* layer = fout->AdditionalLayer(i - 1, direction);
                                           return layer->Value(j, N3+k);
                                       }
                                       return fout->operator()(index);
                                   });
            SLAE::TridiagonalMatrixParallelSolver
            solverNorm(l, c, r, d,
                       std::unique_ptr<SLAE::TrigiagonalEquations>(new SLAE::TrigiagonalEquations(l0, c0, r0, d0)),
                       thread->Commutation(direction));
            solverNorm.Solve(&x, &z);
            for (int i = 0; i < N; ++i) {
                index.SetI(i, direction);
                fout->operator[](index) = x[i];
            }
        }
    }
}
