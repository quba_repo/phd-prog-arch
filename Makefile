CC=mpicxx
CFLAGS= -D PARALLEL_MODE
LDFLAGS=
SOURCES= main.cpp grid_function.cpp data_source.cpp mpi_utils.cpp factorization.cpp 
OBJECTS= $(SOURCES:.cpp=.o)
EXECUTABLE= parallel.out

all:
	$(CC) $(CFLAGS) $(SOURCES) -o $(EXECUTABLE)

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm -f *.o $(EXECUTABLE)
	rm -fr $(EXECUTABLE)*
