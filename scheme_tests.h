#ifndef scheme_tests_h
#define scheme_tests_h
#include "factorization.h"

void TestMatrixRevert(std::shared_ptr<Control::Thread> threadPtr) {
    Index rankOffset = threadPtr->GetRankOffset();
    const DataSource ds = threadPtr->GetDataSource();
    for (int direction = 1; direction < 3; direction++) {
        ArrayedFuncShared fout = ArrayedFuncShared(new ArrayedFunc(GridShared(new Grid(GridNormal, ds, 'p', rankOffset)), ds, 'p'));
        CharacteristicFShared charF = CharacteristicFShared(new CharacteristicFunction(direction, fout->GetGridShared()));
        auto expr = DerFD(DerFD(charF, direction), direction);
        
        RevertMatrix(expr, charF, fout, threadPtr, direction);
    }
}

void TestScheme(const FactorizationScheme& scheme, std::shared_ptr<Control::Thread> thread) {
    TestMatrixRevert(thread);
}

#endif /* scheme_tests_h */
