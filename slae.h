#ifndef slae_h
#define slae_h

#pragma once
#include <vector>
#include <memory>
#include <exception>
#include <cmath>

//
// Systems of Linear algebraic equations
// And their solvers
//
namespace SLAE {
    // abstract algebraic system
    // Ax = D
    //
    class AlgebraicSystem {
     public:
        explicit AlgebraicSystem(const std::vector<double>& d) {
            d_ = std::vector<double>(d);
            size_ = d_.size();
        }
        size_t size() const {
            return size_;
        }
        virtual double A(int i, int j) = 0;
        double D(int i) const {
            if (i >= 0 && i < (int)size()) {
                return d_[i];
            }
            std::cerr << "SLAE:Trying to take D in " << i << " of " << size();
            std::cerr << std::endl;
            std::terminate();
            return -1;
        }
        // helper setter for taking system, which has limit dimensional
        //
        void SetLimit(size_t limit) {
            if (limit <= d_.size()) {
                size_ = limit;
            }
            else {
                std::cerr << "SLAE:trying to set limit more than size";
            }
        }
     private:
        std::vector<double> d_;
        size_t size_;
    };
    
    // trigiagonal matrix equations
    // l*(x-) + c*x + r*(x+) = d
    //
    class TrigiagonalEquations : public AlgebraicSystem {
     public:
        TrigiagonalEquations(const std::vector<double>& l,
                             const std::vector<double>& c,
                             const std::vector<double>& r,
                             const std::vector<double>& d,
                             bool cyclic = false)
        : AlgebraicSystem(d), cyclic_(cyclic) {
            if (l.size() != r.size() || l.size() != c.size()
                || c.size() != r.size() || l.size() != d.size()) {
                std::cerr << "Trying to create trigiagonal SLAE ";
                std::cerr << "with dimensions: l[" << l.size() << "], c[";
                std::cerr << c.size() << "], r[" << r.size() << "], d[";
                std::cerr << d.size() << "]." << std::endl;
                std::terminate();
            }
            l_ = std::vector<double>(l);
            r_ = std::vector<double>(r);
            c_ = std::vector<double>(c);
        }
        virtual double A(int i, int j) {
            int N = (int)size();
            if (cyclic_) {
                if (i == 0 && j == N - 1) {
                    return l_[0];
                } else if (i == N - 1 && j == 0) {
                    return r_[size() - 1];
                }
            }
            if (i >= 0 && j >= 0 && i < N && j < N) {
                if (i == j) {
                    return c_[i];
                } else if (i - 1 == j) {
                    return l_[i];
                } else if (i + 1 == j) {
                    return r_[i];
                }
                return 0;
            }
            std::cerr << "TDSE:Trying to take A[" << i << "," << j << "] ";
            std::cerr << "with size " << size() << std::endl;
            std::terminate();
            return -1;
        }
        bool isCyclic() const {
            return cyclic_;
        }
     protected:
        std::vector<double> l_;
        std::vector<double> c_;
        std::vector<double> r_;
        bool cyclic_;
    };
    
    // Abstract solver class
    //
    class Solver {
     public:
        virtual void Solve(std::vector<double>* x) const = 0;
    };
    
    // Tridiagonal matrix solver
    //
    class TridiagonalMatrixSolver : public Solver {
     public:
        explicit TridiagonalMatrixSolver(std::unique_ptr<TrigiagonalEquations> tdae) {
            tdae_ = std::move(tdae);
        }
        void Solve(std::vector<double>* x) const {
            const double eps = 1e-15;
            const int N = (int)tdae_->size();
            if (N < 3) {
                std::cerr << "TDMASolver has small matrix size: " << N;
                std::cerr << std::endl;
                std::terminate();
            }
            if ((int)x->size() != N) {
                x->resize(N);
            }
            if (tdae_->isCyclic()) {
                SolveCyclic_(x);
                return;
            }
            double* alp = new double[N];
            double* beta = new double[N];

            alp[1] = -tdae_->A(0, 1);
            if (fabs(alp[1]) > eps) {
                if (fabs(tdae_->A(0, 0)) < eps) {
                    std::cerr << "TDMASolver: a[0, 0] == " << tdae_->A(0, 0);
                    std::cerr << " is too small" << std::endl;
                    std::terminate();
                }
                alp[1] /= tdae_->A(0, 0);
            }
            beta[1] = tdae_->D(0);
            if (fabs(beta[1]) > eps) {
                if (fabs(tdae_->A(0, 0)) < eps) {
                    std::cerr << "TDMASolver: a[0, 0] == " << tdae_->A(0, 0);
                    std::cerr << " is too small" << std::endl;
                    std::terminate();
                }
                beta[1] /= tdae_->A(0, 0);
            }
            
            for(int i = 1; i < N - 1; ++i) {
                double ratio = (tdae_->A(i, i) + tdae_->A(i, i-1)*alp[i]);
                if (fabs(ratio) < eps) {
                    std::cerr << "TDMASolver: ratio for alp, beta == " << ratio;
                    std::cerr << " is too small" << std::endl;
                    std::terminate();
                }
                alp[i+1] = - tdae_->A(i, i+1)/ratio;
                beta[i+1]= (tdae_->D(i) - tdae_->A(i, i-1)*beta[i])/ratio;
            }
            
            (*x)[N-1] = (tdae_->D(N-1) - tdae_->A(N-1, N-2) * beta[N-1]);
            double ratio = (tdae_->A(N-1, N-1) + tdae_->A(N-1, N-2) * alp[N-1]);
            if (fabs((*x)[N-1]) > eps) {
                if (fabs(ratio) < eps) {
                    std::cerr << "TDMASolver: ratio for x[N] == " << ratio;
                    std::cerr << " is too small" << std::endl;
                    std::terminate();
                }
                (*x)[N-1] /= ratio;
            }
            for(int i = N - 1; i > 0; --i) {
                (*x)[i-1] = alp[i]*(*x)[i] + beta[i];
            }

            delete [] alp;
            delete [] beta;
        }
     private:
        void SolveCyclic_(std::vector<double>* x) const {
            const double eps = 1e-15;
            const int N = (int)tdae_->size();
            
            double* w = new double[N + 1];
            double* v = new double[N + 1];
            double* alp = new double[N + 1];
            double* beta = new double[N + 1];
            double* gamma = new double[N + 1];
            
            alp[1] = 0;
            beta[1] = 0;
            gamma[1] = 1;
            
            v[N] = 0;
            w[N] = 1;
            
            for(int i = 1; i < N; ++i) {
                double ratio = (tdae_->A(i, i) + tdae_->A(i, i-1)*alp[i]);
                if (fabs(ratio) < eps) {
                    std::cerr << "TDMACySolver: ratio for alp, beta, gamma == ";
                    std::cerr << ratio << " is too small" << std::endl;
                    std::terminate();
                }
                alp[i+1] = i+1 == N
                ? -tdae_->A(i, 0)/ratio
                : -tdae_->A(i, i+1)/ratio;
                beta[i+1] = (tdae_->D(i) - tdae_->A(i, i-1)*beta[i])/ratio;
                gamma[i+1] = - tdae_->A(i, i-1)*gamma[i]/ratio;
            }
            
            for(int i = N; i > 0; --i) {
                v[i-1] = alp[i]*v[i] + beta[i];
                w[i-1] = alp[i]*w[i] + gamma[i];
            }
            
            double u0 = (tdae_->D(0) - tdae_->A(0, N-1)*v[N-1] - tdae_->A(0, 1)*v[1]);
            double ratio = tdae_->A(0, N-1)*w[N-1] + tdae_->A(0, 0) + tdae_->A(0, 1)*w[1];
            if (fabs(u0) > eps) {
                if (fabs(ratio) < eps) {
                    std::cerr << "TDMACySolver: ratio for u0 == ";
                    std::cerr << ratio << " is too small" << std::endl;
                    std::terminate();
                }
                u0 /= ratio;
            }
            for (int i = 0; i < N; ++i) {
                (*x)[i] = v[i] + u0 * w[i];
            }
            delete [] alp;
            delete [] beta;
            delete [] gamma;
            delete [] v;
            delete [] w;
        }
        std::unique_ptr<TrigiagonalEquations> tdae_;
    };
}

#endif /* slae_h */
