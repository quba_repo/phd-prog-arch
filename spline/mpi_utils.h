#ifndef mpi_utils_h
#define mpi_utils_h

#pragma once
#include <vector>
#include <mpi.h>
//
namespace Utils {
    // utils methods
    //
    void FilePrint(MPI_Comm commutation, const std::stringstream& stream,
                   const std::string& filename,
                   bool barrier = true);
    void SerialPrint(MPI_Comm commutation, const std::stringstream& stream,
                     bool barrier = true);
    void ArrayPrint(MPI_Comm commutation, const std::vector<double>& v,
                    const std::string& name, bool barrier = true);
    void ArrayPrint(MPI_Comm commutation, double* v, int N,
                    const std::string& name, bool barrier = true);
    template <class _lDataSource, class _cDataSource,
        class _rDataSource, class _dDataSource>
    void FillDataForSlae(MPI_Comm commutation, int Count,
                         std::vector<double>* l, std::vector<double>* c,
                         std::vector<double>* r, std::vector<double>* d,
                         std::vector<double>* l0, std::vector<double>* c0,
                         std::vector<double>* r0, std::vector<double>* d0,
                         _lDataSource lSource, _cDataSource cSource,
                         _rDataSource rSource, _dDataSource dSource) {
        l->resize(Count);
        c->resize(Count);
        r->resize(Count);
        d->resize(Count);
        int rank, size;
        MPI_Comm_rank(commutation, &rank);
        MPI_Comm_size(commutation, &size);
        for (int i = 0; i < Count; ++i) {
            int loc = rank*(Count - 1) + i;
            (*l)[i] = lSource(i, loc, rank, size);
            (*c)[i] = cSource(i, loc, rank, size);
            (*r)[i] = rSource(i, loc, rank, size);
            (*d)[i] = dSource(i, loc, rank, size);
        }
        if (rank == 0) {
            l0->resize(size+1);
            c0->resize(size+1);
            r0->resize(size+1);
            d0->resize(size+1);
            for (int i=0; i <= size; ++i) {
                int s = (Count-1)*i;
                (*l0)[i] = lSource(i, s, rank, size);
                (*c0)[i] = cSource(i, s, rank, size);
                (*r0)[i] = rSource(i, s, rank, size);
                (*d0)[i] = dSource(i, s, rank, size);
            }
        }
    }
}

#endif /* mpi_utils_h */
