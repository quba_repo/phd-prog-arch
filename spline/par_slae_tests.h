#ifndef par_slae_tests_h
#define par_slae_tests_h

#pragma once
#include <memory>

#include "asserts.h"
#include "slae-parallel.h"
#include "mpi_utils.h"

#define Xnum 10
#define Ynum 3
#define Znum 15

class Tester {
public:
    int minX;
    int maxX;
    int minY;
    int maxY;
    int minZ;
    int maxZ;
    Tester(const Control::Thread& thread) {
        minX = Xnum*thread.RankX();
        maxX = minX + Xnum - 1;
        
        minY = Ynum*thread.RankY();
        maxY = minY + Ynum - 1;
        
        minZ = Znum*thread.RankZ();
        maxZ = minZ + Znum - 1;
    };
    
    void Test(const Control::Thread& thread) {
        if (thread.Px() > 1 ) {
            if (thread.RankX() == 0) {
                ASSERT_EQ(0, minX);
                for (int i = 1; i < thread.Px(); ++i) {
                    int minnX;
                    MPI_Status status;
                    MPI_Recv(&minnX, 1, MPI_INTEGER, i, 10, thread.Commutation(1), &status);
                    ASSERT_EQ(Xnum*i, minnX);
                }
            }
            else {
                MPI_Send(&minX, 1, MPI_INTEGER, 0, 10, thread.Commutation(1));
            }
        }
        if (thread.Py() > 1) {
            if (thread.RankY() == 0) {
                ASSERT_EQ(0, minY);
                for (int i = 1; i < thread.Py(); ++i) {
                    int minnY;
                    MPI_Status status;
                    MPI_Recv(&minnY, 1, MPI_INTEGER, i, 12, thread.Commutation(2), &status);
                    ASSERT_EQ(Ynum*i, minnY);
                }
            }
            else {
                MPI_Send(&minY, 1, MPI_INTEGER, 0, 12, thread.Commutation(2));
            }
        }
        if (thread.Pz() > 1) {
            if (thread.RankZ() == 0) {
                ASSERT_EQ(0, minZ);
                for (int i = 1; i < thread.Pz(); ++i) {
                    int minnZ;
                    MPI_Status status;
                    MPI_Recv(&minnZ, 1, MPI_INTEGER, i, 14, thread.Commutation(3), &status);
                    ASSERT_EQ(Znum*i, minnZ);
                }
            }
            else {
                MPI_Send(&minZ, 1, MPI_INTEGER, 0, 14, thread.Commutation(3));
            }
        }
    }
private:
    std::unique_ptr<Control::Thread> thread_;
};

void testSlae(const Control::Thread& thread, int N) {
    if (thread.RankY() == 0 && thread.RankZ() == 0) {
        std::vector<double> l, c, r, d, x;
        ASSERT_EQ(0, x.size());
        std::vector<double> l0, c0, r0, d0;
        int iTest;
        Utils::FillDataForSlae(thread.Commutation(1), N,
                               &l, &c, &r, &d,
                               &l0, &c0, &r0, &d0,
                               [&](int i, ...){
                                   iTest = i;
                                   return 1;
                               },
                               [&](int i, ...){
                                   ASSERT_EQ(i, iTest);
                                   return 2;
                               },
                               [&](int i, ...){
                                   ASSERT_EQ(i, iTest);
                                   return 1;
                               },
                               [&](int idx, int i, int rank, int size) {
                                   ASSERT_EQ(idx, iTest);
                                   int last = size*(N - 1);
                                   if (i == last) {
                                       return 3*last - 1;
                                   }
                                   else if (i == 0) {
                                       return 1;
                                   }
                                   return 4*i;
                               });
        SLAE::TridiagonalMatrixParallelSolver solverNorm(l, c, r, d, std::unique_ptr<SLAE::TrigiagonalEquations>(new SLAE::TrigiagonalEquations(l0, c0, r0, d0)), thread.Commutation(1));
        solverNorm.Solve(&x);
        ASSERT_EQ(N, x.size());
        for (int i=0; i < x.size(); i++) {
            ASSERT_DOBL_EQ((double)((N - 1)*thread.RankX() + i), x[i]);
        }
    }
}

void testSlaeCyclic(const Control::Thread& thread, int N) {
    if (thread.RankY() == 0 && thread.RankZ() == 0) {
        std::vector<double> l, c, r, d;
        std::vector<double> l0, c0, r0, d0;
        Utils::FillDataForSlae(thread.Commutation(1), N,
                               &l, &c, &r, &d,
                               &l0, &c0, &r0, &d0,
                               [](...){ return 1; },
                               [](...){ return 3; },
                               [](...){ return 1; },
                               [&N](int idx, int i, int rank, int size) {
                                   int last = size*(N - 1);
                                   if (i > 0 && i < last - 1) {
                                       return i + 3*(i + 1) + i + 2;
                                   }
                                   else if (i == last - 1) {
                                       return i + 3*(i + 1) + 1;
                                   }
                                   return last + 3 + 2;
                               });
        std::vector<double> x, res;
        for (int i=0; i < N; i++) {
            res.push_back(thread.RankX()*(N - 1) + i + 1);
        }
        if (thread.RankX() == thread.Px() - 1) {
            res[N-1] = 1;
        }
        SLAE::TridiagonalMatrixParallelSolver solverNorm(l, c, r, d, std::unique_ptr<SLAE::TrigiagonalEquations>(new SLAE::TrigiagonalEquations(l0, c0, r0, d0, true)), thread.Commutation(1));
        solverNorm.Solve(&x);
        ASSERT_EQ(N, x.size());
        for (int i=0; i < x.size(); i++) {
            ASSERT_DOBL_EQ((double)res[i], x[i]);
        }
    }
}

void TestSendRecv() {
    
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    
    if (size == 1) {
        return;
    }
    
    int N = 5;
    double* r = new double[N];
    double* l = new double[N];
    for (int i= 0 ; i < N; i++) {
        r[i] = 100*i + rank;
        l[i] = 1000*i + rank;
    }
    
    int destLeft = rank - 1;
    int destRight = rank + 1;
    
    if (rank == 0) {
        destLeft = size - 1;
    }
    if (rank == size - 1) {
        destRight = 0;
    }
    
    MPI_Status lStatus, rStatus;
    
    if (rank % 2 == 0) {
        MPI_Sendrecv_replace(l, N, MPI_DOUBLE,
                             destLeft, kGFTagLeftInner,
                             destLeft, kGFTagRightInner,
                             MPI_COMM_WORLD, &lStatus);
        MPI_Sendrecv_replace(r, N, MPI_DOUBLE,
                             destRight, kGFTagRightInner,
                             destRight, kGFTagLeftInner,
                             MPI_COMM_WORLD, &rStatus);
    } else {
        MPI_Sendrecv_replace(r, N, MPI_DOUBLE,
                             destRight, kGFTagRightInner,
                             destRight, kGFTagLeftInner,
                             MPI_COMM_WORLD, &rStatus);
        MPI_Sendrecv_replace(l, N, MPI_DOUBLE,
                             destLeft, kGFTagLeftInner,
                             destLeft, kGFTagRightInner,
                             MPI_COMM_WORLD, &lStatus);
    }
    
    for (int i= 0 ; i < N; i++) {
        ASSERT_DOBL_EQ(100*i + destLeft, l[i]);
        ASSERT_DOBL_EQ(1000*i + destRight, r[i]);
    }
    
    delete [] r;
    delete [] l;
}

void LaunchSLAETests(const Control::Thread& thread) {
    Tester tester(thread);
    tester.Test(thread);
    for (int i = 4; i < 100; i++) {
        testSlae(thread, i);
        testSlaeCyclic(thread, i);
    }
    TestSendRecv();
}

#endif /* par_slae_tests_h */
