#include <math.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <stdio.h>
#include <mpi.h>

#include "slae-parallel.h"
#include "mpi_utils.h"

#define mD  100
#define PI  M_PI
#define PI2 2*M_PI

#define B 2

#define L 1.
#define R -1.

typedef std::vector<double> darray;

double f(double x)
{
    return sin(x);
}

double spline(int fi, int n, double v, const darray& x, const darray& y, const darray& u)
{
    const int N = mD;
    int i = fi;
    for (int j = fi; j < fi + n; j++) {
        if (x[j] >= v) {
            i = j;
            break;
        }
        if (j == (fi + n - 1) ) {
            i = j;
        }
    }
    if (i == 0) {
        i++;
    }
    int ii = i - fi;
    const double l = x[i] - v;
    const double r = v - x[i - 1];
    const double h = x[i] - x[i - 1];
    return y[i-1]*l/h + y[i]*r/h
    + u[ii-1]*(pow(l, 3) - pow(h, 2)*l)/(6*h)
    + u[ii]*(pow(r, 3) - pow(h, 2)*r)/(6*h);
}

int main(int argc, char* argv[]) {
    
    MPI_Init(NULL, NULL);
    if (argc != 4) {
        std::cerr << "must be initialized 3 proc. parameters Px*Py*Pz = N";
        std::cerr << std::endl;
        return -1;
    }
    int Px, Py, Pz;
    sscanf(argv[1], "%d", &Px);
    sscanf(argv[2], "%d", &Py);
    sscanf(argv[3], "%d", &Pz);
    
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    
    int m = mD;
    double* a = new double[m + 1];
    double* b = new double[m + 1];
    double* cr = new double[m + 1];
    double* dr = new double[m + 1];
    
    darray x; x.resize(m+1);
    darray y; y.resize(m+1);
    double* h = new double[m + 1];
    double H = B/(double)m;
    for (int i=0; i <= m; i++) {
        x[i] = i*H;
        y[i] = f(x[i]);
        if (i > 0) {
            h[i] = x[i] - x[i-1];
        }
    }
    
    for (int i=1; i < m; i++) {
        a[i] = h[i]/6.;
        cr[i] = (h[i]+h[i+1])/3.;
        b[i] = h[i+1]/6.;
        dr[i] = (y[i+1] - y[i])/h[i+1] - (y[i] - y[i-1])/h[i];
    }
    
    double lb = 3.*((y[1]-y[0])/h[1] - L)/h[1];
    double rb = 3.*(R - (y[m]-y[m-1])/h[m])/h[m];
    
    int n = m/Px + 1;
    
    std::vector<double> l, c, r, d, res;
    std::vector<double> l0, c0, r0, d0;
    
    Utils::FillDataForSlae(MPI_COMM_WORLD, n,
                           &l, &c, &r, &d,
                           &l0, &c0, &r0, &d0,
                           [&](int i, int j, int rank, int size){
                               int last = size*(n - 1);
                               if (j == last) {
                                   return 0.5;
                               }
                               if (j == 0) {
                                   return 0.0;
                               }
                               return a[j];
                           },
                           [&](int i, int j, int rank, int size){
                               int last = size*(n - 1);
                               if (j == last || j == 0) {
                                   return 1.;
                               }
                               return cr[j];
                           },
                           [&](int i, int j, int rank, int size){
                               int last = size*(n - 1);
                               if (j == 0) {
                                   return 0.5;
                               }
                               if (j == last) {
                                   return 0.0;
                               }
                               return b[j];
                           },
                           [&](int idx, int j, int rank, int size) {
                               int last = size*(n - 1);
                               if (j == last) {
                                   return rb;
                               }
                               else if (j == 0) {
                                   return lb;
                               }
                               return dr[j];
                           });
    std::unique_ptr<SLAE::TrigiagonalEquations> ptr(new SLAE::TrigiagonalEquations(l0, c0, r0, d0));
    SLAE::TridiagonalMatrixParallelSolver solverNorm(l, c, r, d, std::move(ptr), MPI_COMM_WORLD);
    solverNorm.Solve(&res);
    
    std::stringstream ss;
    if (rank == 0) {
        ss << "list = {\n";
    }
    double BR = x[(rank + 1)*(n - 1)];
    double v=x[rank*(n - 1)];
    for (; v<=BR;) {
        if (v > 0) {
            ss << ",\n";
        }
        double resv = spline(rank*(n - 1), n, v, x, y, res);
        ss << "\t{" << v << ",\t" << resv << "}";
        v += 0.01;
    }
    if (rank == Px - 1) {
        ss << "}\n Show[ListLinePlot[list, PlotStyle -> Black], Plot[x*x*x*x/20000 + Sin[x], {x, 0,"
        << BR << "}]]\n";
    }
    Utils::FilePrint(MPI_COMM_WORLD, ss, "result.nb");
    
    std::cerr << "im here" << rank << std::endl;
    
    delete [] h;
    delete [] a;
    delete [] b;
    delete [] cr;
    delete [] dr;
    
    MPI_Finalize();
    
    return 0;
}
