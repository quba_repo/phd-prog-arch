//
//  index.h
//  partest
//
//  Created by Антон Кудряшов on 14/12/16.
//  Copyright © 2016 Антон Кудряшов. All rights reserved.
//

#ifndef index_h
#define index_h

#include "grid_type.h"
#pragma once
#include <iostream>
#include <sstream>

struct Index {
    int i, j, k;
    Index() : Index(0, 0, 0) {};
    Index(int _i, int _j, int _k) : i(_i), j(_j), k(_k) {};
    virtual std::string Description() const {
        std::stringstream ss;
        ss << "{" << i << ", " << j << ", " << k << "}";
        return ss.str();
    }
    void SetI(int ii, int direction) {
        switch (direction) {
            case 1:
                i = ii;
                return;
            case 2:
                j = ii;
                return;
            case 3:
                k = ii;
                return;
            default:
                break;
        }
        std::cerr << "Set:I for " << direction << " doesn't exist" << std::endl;
        std::terminate();
    }
    int GetI(int direction) const {
        switch (direction) {
            case 1:
                return i;
            case 2:
                return j;
            case 3:
                return k;
            default:
                break;
        }
        std::cerr << "Get:I for " << direction << " doesn't exist" << std::endl;
        std::terminate();
    }
};

struct GridIndex : public Index {
    // shift is -1/2 for indexes
    bool shiftX, shiftY, shiftZ;
    bool Shift(int direction) const {
        switch (direction) {
            case 1:
                return shiftX;
            case 2:
                return shiftY;
            case 3:
                return shiftZ;
            default:
                break;
        }
        std::cerr << "Get:Shift for " << direction << " doesn't exist" << std::endl;
        std::terminate();
        return false;
    };
    void SetShift(bool shift, int direction) {
        switch (direction) {
            case 1:
                shiftX = shift;
                return;
            case 2:
                shiftY = shift;
                return;
            case 3:
                shiftZ = shift;
                return;
            default:
                break;
        }
        std::cerr << "Set:Shift for " << direction << " doesn't exist" << std::endl;
        std::terminate();
    }
    GridIndex() : GridIndex(0, 0, 0, false, false, false) {}
    GridIndex(int _i, int _j, int _k, bool _shiftX, bool _shiftY, bool _shiftZ)
    : Index(_i, _j, _k), shiftX(_shiftX), shiftY(_shiftY), shiftZ(_shiftZ) {
        rankP = -1;
        directionP = 0;
    };
    std::string Description() const {
        std::stringstream ss;
        ss << Index::Description() << ", sh[" << shiftX << ", " << shiftY << ", ";
        ss << shiftZ << "], rp[" << rankP << ", " << directionP << "]";
        return ss.str();
    }
    // outer indexes
    int rankP, directionP;
};

#endif /* index_h */
