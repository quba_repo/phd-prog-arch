//
//  file_handler.h
//  partest
//
//  Created by Антон Кудряшов on 25/05/2017.
//  Copyright © 2017 Антон Кудряшов. All rights reserved.
//

#ifndef file_handler_h
#define file_handler_h

#include "macroses.h"
#include "grid_function.h"
#include "reaper.h"
#include <fstream>

class FileHandler {
 public:
    FileHandler(const char* filename, const char* mode) {
        filename_ = filename;
    }
    void PrintStr(const std::string& str) {
        file_ << str;
    }
    void PrintSS(const std::stringstream& ss) {
        PrintStr(ss.str());
    }
    void Open(std::ios_base::openmode mode) {
        file_.open(filename_, mode);
        if (file_.fail()) {
            std::cerr << "File " << filename_ << " is not opened" << std::endl;
            std::terminate();
        }
    }
    void Close() {
        if (!file_.fail()) {
            file_.close();
        }
    }
    
    ~FileHandler() {
        Close();
    }
 private:
    const char* filename_;
    std::fstream file_;
};

class FunctionPrinter : public FileHandler {
 public:
    FunctionPrinter(const char* filename) : FileHandler(filename, "w") {
        //
    }
    void PrintFunction(ArrayedFuncShared func,
                       std::shared_ptr<Control::Thread> thread) {
        MPI_Comm commutation = thread->Commutation(1);
        int rank = thread->RankX();
        bool print = rank == 0;
        if (!print) {
            MPI_Status stat;
            MPI_Recv(&print, 1, MPI_C_BOOL, rank - 1, kFHTagQueue, commutation, &stat);
        }
        Open(rank == 0 ? std::ios::out : std::ios::app);
        GridShared grid = func->GetGridShared();
        int i_N = grid->N_loc(1);
        int j_N = grid->N_loc(2);
        int k_N = grid->N_loc(3);
        GridIndex index;
        
        for (int k = -1; k <= k_N; ++k) {
            std::stringstream sz;
            sz << "z = " << k << " on rankx = " << rank << "\n";
            PrintSS(sz);
            index.k = k;
            for (int j = -1; j <= j_N; ++j) {
                index.j = j;
                for (int i = -1; i <= i_N; ++i) {
                    index.i = i;
                    std::stringstream ss;
                    ss.precision(5);
                    ss << func->operator()(index) << "  ";
                    PrintSS(ss);
                }
                PrintStr("\n");
            }
            PrintStr("\n");
        };
        Close();
        if (rank < thread->Px() - 1) {
            MPI_Send(&print, 1, MPI_C_BOOL, rank + 1, kFHTagQueue, commutation);
        }
    }
    void PrintSliceFunction(ArrayedFuncShared func,
                            std::shared_ptr<Control::Thread> thread,
                            int sliceDirection,
                            int sliceIndex1,
                            int sliceIndex2) {
        int dir2, dir3;
        FindOtherDirections(sliceDirection, &dir2, &dir3);
        
    }
};

#endif /* file_handler_h */
