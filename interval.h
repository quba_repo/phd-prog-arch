//
//  Interval.h
//  partest
//
//  Created by Антон Кудряшов on 14/12/16.
//  Copyright © 2016 Антон Кудряшов. All rights reserved.
//

#ifndef Interval_h
#define Interval_h

struct Interval {
    double a;
    double b;
    double Width() const {
        return b - a;
    }
};

#endif /* Interval_h */
