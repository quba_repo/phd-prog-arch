//
//  layer.h
//  partest
//
//  Created by Антон Кудряшов on 27/02/17.
//  Copyright © 2017 Антон Кудряшов. All rights reserved.
//

#ifndef layer_h
#define layer_h

#include "index.h"
#pragma once
#include <vector>

class BoundLayer {
 public:
    BoundLayer() : BoundLayer(0, 0) {};
    BoundLayer(int N1, int N2) : N1_(N1), N2_(N2) {
        if (N1*N2 > 0) {
            container_ = new double[N1_*N2_];
            for (int i = 0; i < N1_; ++i) {
                for (int j = 0; j < N2_; ++j) {
                    container_[i + N1_*j] = 0;
                }
            }
        }
    };
    double* GetContainer() const {
        return container_;
    }
    int size() const {
        return N1_*N2_;
    }
    void SetValue(int i, int j, double value) {
        CheckIndex(i, j);
        container_[i + N1_*j] = value;
    }
    double Value(int i, int j) const {
        CheckIndex(i, j);
        return container_[i + N1_*j];
    }
    void CheckIndex(int i, int j) const {
        if (i < 0 || i >= N1_ || j < 0 || j >= N2_) {
            std::cerr << "i,j out of range" << i << " " << j << " with ";
            std::cerr << N1_ << " " << N2_ << std::endl;
            std::terminate();
        }
    }
    ~BoundLayer() {
        delete[] container_;
    }
 private:
    int N1_;
    int N2_;
    double* container_;
};

class AdditionalLayers {
 public:
    AdditionalLayers(int Px, int Py, int Pz,
                     int Nx, int Ny, int Nz,
                     Index rankIndex) {
        std::vector<BoundLayer*> layersX, layersY, layersZ;
        if (Px > 1 && rankIndex.i == 0) {
            for (int i=0; i<Px-1; ++i) {
                layersX.push_back(new BoundLayer(Ny, 2*Nz));
            }
        }
        layers_.push_back(layersX);
        if (Py > 1 && rankIndex.j == 0) {
            for (int i=0; i<Py-1; ++i) {
                layersY.push_back(new BoundLayer(Nx, 2*Nz));
            }
        }
        layers_.push_back(layersY);
        if (Pz > 1 && rankIndex.k == 0) {
            for (int i=0; i<Pz-1; ++i) {
                layersZ.push_back(new BoundLayer(Nx, 2*Ny));
            }
        }
        layers_.push_back(layersZ);
    }
    // i from [0, Nx-1], j from [0, 2Ny - 1]
    // N-2 layer [0, Nx-1]x[0,  Ny - 1]
    // N-1 layer [0, Nx-1]x[Ny, 2Ny - 1]
    BoundLayer* Layer(int rank, int direction) const {
        if (direction < 1 || direction > 3) {
            std::cerr << "missed direction for additional layers" << std::endl;
            std::terminate();
        }
        if (rank <= 0) {
            std::cerr << "missed rank in additional layers" << std::endl;
            std::terminate();
        }
        
        return layers_[direction - 1][rank - 1];
    }
 private:
    std::vector< std::vector<BoundLayer*> > layers_;
};

#endif /* layer_h */
