#ifndef factorization_hpp
#define factorization_hpp

#include "thread.h"
#include "grid_function.h"
#include "reaper.h"

struct Functions {
    ArrayedFuncShared u;
    ArrayedFuncShared v;
    ArrayedFuncShared w;
    ArrayedFuncShared p;
    
    double FindMaxError(Reaper reaper, char* symbol);
};

class FactorizationScheme {
 public:
    FactorizationScheme(std::shared_ptr<Control::Thread> threadPtr);
    ~FactorizationScheme() {};
    void Solve();
 private:
    void Step0_(Functions ksi);
    void StepPuasson_(Functions ksi);
    void StepX_(Functions ksi);
    void StepY_(Functions ksi);
    void StepZ_(Functions ksi);
    void StepFinal_(const Functions& ksi);
    Reaper GetReaper() const;
    double SolvePuasson_(const Functions& ksi, ArrayedFuncShared ksi_p_0);
    bool IterationCondition_(bool globals, int iterations, double distance);
    Functions f_;
    GridFunctionShared mu_;
    std::shared_ptr<Control::Thread> threadPtr_;
};

void RevertMatrix(GridFunctionShared expr,
                  CharacteristicFShared chf,
                  ArrayedFuncShared fout,
                  std::shared_ptr<Control::Thread> thread,
                  int direction);

#endif /* factorization_hpp */
