#!/bin/bash
(( total = $1*$2*$3 ))
echo "Number of procs: $1 $2 $3; Total: $total"
make clean
echo "Clean done"
make
echo "Make done. Let's rock"
orterun -np $total ./a.out $1 $2 $3
