#ifndef slae_par_h
#define slae_par_h

#pragma once
#include <mpi.h>

#include "slae.h"

//
// Systems of Linear algebraic equations
// And their solvers for MPI parallel version
//
namespace SLAE {
    class TridiagonalMatrixParallelSolver : public Solver {
     public:
        TridiagonalMatrixParallelSolver(const std::vector<double>& l,
                                        const std::vector<double>& c,
                                        const std::vector<double>& r,
                                        const std::vector<double>& d,
                                        std::unique_ptr<TrigiagonalEquations> tdae0,
                                        MPI_Comm commutation)
        : commutation_(commutation) {
            int size;
            MPI_Comm_size(commutation, &size);
            isCyclic_ = tdae0->isCyclic();
            if (size == 0) {
                std::cerr << "TridiagonalMatrixParallelSolver: ";
                std::cerr << "commutation is empty" << std::endl;
                std::terminate();
            }
            else if (size == 1) {
                std::unique_ptr<TrigiagonalEquations> system = std::unique_ptr<TrigiagonalEquations>(new TrigiagonalEquations(l, c, r, d, isCyclic_));
                if (isCyclic_) {
                    system->SetLimit(l.size() - 1);
                }
                oneSolverCase_ = std::unique_ptr<TridiagonalMatrixSolver>(new TridiagonalMatrixSolver(std::move(system)));
                return;
            }
            std::vector<double> l_0(l), c_0(c), r_0(r);
            if (l_0.size() == 0 || c_0.size() == 0 || r_0.size() == 0) {
                std::cerr << "TridiagonalMatrixParallelSolver: ";
                std::cerr << "l,c,r must have >= 3 items" << std::endl;
                MPI_Abort(commutation, 0);
            }
            l_0[0] = c_0[0] = r_0[0] = 0;
            l_0.back() = c_0.back() = r_0.back() = 0;
            std::vector<double> d_0;
            d_0.resize(d.size());
            // split into 3 slae systems
            // A(u + v + w) = d
            // Au = 0, u_0 = 1, u_N = 0,
            c_0[0] = d_0[0] = 1;
            TridiagonalMatrixSolver uSolver = TridiagonalMatrixSolver(std::unique_ptr<TrigiagonalEquations>(new TrigiagonalEquations(l_0, c_0, r_0, d_0)));
            uSolver.Solve(&u_);
            // Av = 0, v_0 = 0, v_N = 1,
            d_0[0] = 0;
            c_0.back() = d_0.back() = 1;
            TridiagonalMatrixSolver vSolver = TridiagonalMatrixSolver(std::unique_ptr<TrigiagonalEquations>(new TrigiagonalEquations(l_0, c_0, r_0, d_0)));
            vSolver.Solve(&v_);
            // Aw = d, w_0 = w_N = 0;
            d_0 = std::vector<double>(d);
            d_0.back() = d_0[0] = 0;
            c_0.back() = 1;
            TridiagonalMatrixSolver wSolver = TridiagonalMatrixSolver(std::unique_ptr<TrigiagonalEquations>(new TrigiagonalEquations(l_0, c_0, r_0, d_0)));
            wSolver.Solve(&w_);
            // parallel part
            tdae0_ = std::move(tdae0);
        }
        // Full system have sum of x.
        //
        void Solve(std::vector<double>* x) const {
            std::vector<double> z;
            Solve(x, &z);
        }
        void Solve(std::vector<double>* x, std::vector<double>* z) const {
            if (oneSolverCase_ != NULL) {
                oneSolverCase_->Solve(x);
                if (isCyclic_) {
                    x->push_back((*x)[0]);
                }
                return;
            }
            const size_t N = u_.size();

            int size, rank;
            MPI_Comm_size(commutation_, &size);
            MPI_Comm_rank(commutation_, &rank);
            if (size < 3 && isCyclic_) {
                std::cerr << "TridiagonalMatrixParallelSolver: ";
                std::cerr << "for solving in cyclic parallel mode, we need ";
                std::cerr << "to have 3 or more processors in commutator. ";
                std::cerr << "Current value is " << size << std::endl;
                std::terminate();
            }
            
            double buff[6];
            
            double u_Nm, v_Nm, w_Nm;
            buff[0] = u_[1];
            buff[1] = u_Nm = u_[N-2];
            buff[2] = v_[1];
            buff[3] = v_Nm = v_[N-2];
            buff[4] = w_[1];
            buff[5] = w_Nm = w_[N-2];
            
            const int tagInput = 5;
            const int tagOutput = 6;
            double z_buff[2];
            
            //send and recieve bounds values between processors
            if(rank != 0) {
                MPI_Send(buff, 6, MPI_DOUBLE, 0, tagInput, commutation_);
                MPI_Status status;
                MPI_Recv(z_buff, 2, MPI_DOUBLE, 0, tagOutput, commutation_, &status);
            }
            else {
                std::vector<double> L, C, R, D;
                // left side bounds
                L.push_back(0); //stub. there is must be some value for cyclic
                C.push_back(tdae0_->A(0, 0) + tdae0_->A(0, 1)*u_[1]);
                R.push_back(tdae0_->A(0, 1)*v_[1]);
                D.push_back(tdae0_->D(0) - tdae0_->A(0, 1)*w_[1]);
                
                for (int i=1; i < size; ++i) {
                    MPI_Status status;
                    MPI_Recv(buff, 6, MPI_DOUBLE, i, tagInput, commutation_, &status);
                    L.push_back(tdae0_->A(i, i-1)*u_Nm);
                    C.push_back(tdae0_->A(i, i-1)*v_Nm + tdae0_->A(i, i) + tdae0_->A(i, i+1)*buff[0]);
                    R.push_back(tdae0_->A(i, i+1)*buff[2]);
                    D.push_back(tdae0_->D(i) - tdae0_->A(i, i-1)*w_Nm - tdae0_->A(i, i+1)*buff[4]);
                    u_Nm = buff[1];
                    v_Nm = buff[3];
                    w_Nm = buff[5];
                }
                if (isCyclic_) {
                // left bounds correction for cyclic issue
                //
                //  A[0] = a_0[0]*u_Nm;
                    L[0] = tdae0_->A(0, size)*u_Nm;
                //  C[0] = a_0[0]*v_Nm + c_0[0] + b_0[0]*u[1];
                    C[0] += tdae0_->A(0, size)*v_Nm;
                //  D[0] = d_0[0] - a_0[0]*w_Nm - b_0[0]*w[1];
                    D[0] -= tdae0_->A(0, size)*w_Nm;
                } else {
                // right side
                // is case of cyclic system, we already prepared our tdae.
                // because last point is the same with start point
                    L.push_back(tdae0_->A(size, size - 1)*u_Nm);
                    C.push_back(tdae0_->A(size, size - 1)*v_Nm + tdae0_->A(size, size));
                    R.push_back(0); //stub. there is must be some value for cyclic
                    D.push_back(tdae0_->D(size) - tdae0_->A(size, size - 1)*w_Nm);
                }
                
                TridiagonalMatrixSolver solverZ = TridiagonalMatrixSolver(std::unique_ptr<TrigiagonalEquations>(new TrigiagonalEquations(L, C, R, D, tdae0_->isCyclic())));
                solverZ.Solve(z);
                for (int i=1; i < size; ++i) {
                    z_buff[0] = (*z)[i];
                    z_buff[1] = isCyclic_ && i == size - 1 ? (*z)[0] : (*z)[i + 1];
                    MPI_Send(z_buff, 2, MPI_DOUBLE, i, tagOutput, commutation_);
                }
                z_buff[0] = (*z)[0];
                z_buff[1] = (*z)[1];
            }
            if (x->size() != N) {
                x->resize(N);
            }
            for (int i = 0; i < N; ++i) {
                (*x)[i] = z_buff[0]*u_[i] + z_buff[1]*v_[i] + w_[i];
            }
        }
     private:
        bool isCyclic_;
        std::vector<double> u_;
        std::vector<double> v_;
        std::vector<double> w_;
        std::unique_ptr<TrigiagonalEquations> tdae0_;
        MPI_Comm commutation_;
        std::unique_ptr<TridiagonalMatrixSolver> oneSolverCase_;
    };
}

#endif /* slae_par_h */
